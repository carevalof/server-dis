<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ControllerDistribuccion;
use App\Http\Controllers\aplicacionController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'prefix' => 'app'

], function () {


    Route::get('getCargaUsuario/{idUsuario}/{idEmpresa}', [aplicacionController::class, 'getCargaUsuario']);
    Route::get('loginApp/{usuario}/{pass}', [aplicacionController::class, 'loginApp']);
    Route::get('getOrdenesFechaEmpleado/{idEmpresa}/{idEmpleado}/{fechaInicio}/{fechaFin}', [aplicacionController::class, 'getOrdenesFechaEmpleado']);
    Route::post('crearVentaAuto', [aplicacionController::class, 'crearVentaAuto']);
    Route::post('createcliente', [aplicacionController::class, 'createcliente']);
    Route::post('createclienteVisita', [aplicacionController::class, 'createclienteVisita']);
    
    Route::get('getProducto/{idEmpresa}', [aplicacionController::class, 'getProducto']);
    Route::get('getProductoStock/{idEmpresa}', [aplicacionController::class, 'getProductoStock']);
    
    Route::post('crearCompra', [aplicacionController::class, 'crearCompra']);
    Route::get('getItmesOrdenes/{idOrden}', [aplicacionController::class, 'getItmesOrdenes']);
    Route::get('getvehiculos/{idEmpresa}', [aplicacionController::class, 'getvehiculos']);
    Route::get('getAllProductosVehiculo/{idVehiculo}', [aplicacionController::class, 'getAllProductosVehiculo']);
    Route::get('getimpresoras/{idEmpresa}', [aplicacionController::class, 'getimpresoras']);
    Route::post('updateProductosAuto', [aplicacionController::class, 'updateProductosAuto']);
    Route::get('getOrdenesFecha/{idEmpresa}/{tipo}/{fechaInicio}/{fechaFin}', [aplicacionController::class, 'getOrdenesFecha']);
    Route::post('updateCompra', [aplicacionController::class, 'updateCompra']);

    Route::get('getListaVisitaFecha/{idEmpresa}/{fecha}/{fechafin}', [aplicacionController::class, 'getListaVisitaFecha']);


    
    Route::get('getclientes/{idEmpresa}/{latitud}/{longitud}', [aplicacionController::class, 'getclientes']);
    Route::post('createRuta', [aplicacionController::class, 'createRuta']);
    Route::get('getRutas/{idEmpresa}', [aplicacionController::class, 'getRutas']);
    Route::get('getRutasEmpleado/{idEmpresa}/{idEmpleado}', [aplicacionController::class, 'getRutasEmpleado']);
    
    Route::post('createRutaClientes', [aplicacionController::class, 'createRutaClientes']);
    Route::get('getEmpleados/{idEmpresa}', [aplicacionController::class, 'getEmpleados']);
    Route::post('designarEmplieado', [aplicacionController::class, 'designarEmplieado']);
    Route::post('updateRuta', [aplicacionController::class, 'updateRuta']);
    Route::get('getClientesRutas/{idRuta}', [aplicacionController::class, 'getClientesRutas']);

    Route::post('updateRutaCliente', [aplicacionController::class, 'updateRutaCliente']);
    Route::post('updatecliente', [aplicacionController::class, 'updatecliente']);
    Route::post('crearVisita', [aplicacionController::class, 'crearVisita']);
    
    
});


Route::group([

    'middleware' => 'api',
    'prefix' => 'distribuccion'

], function () {

    Route::get('login/{usuario}/{pass}', [ControllerDistribuccion::class, 'login']);
    Route::post('register', [ControllerDistribuccion::class, 'register']);
    Route::get('getallPermisos', [ControllerDistribuccion::class, 'getallPermisos']);
    Route::put('updatePermisos', [ControllerDistribuccion::class, 'updatePermisos']);
    Route::post('createPermisos', [ControllerDistribuccion::class, 'createPermisos']);
    Route::post('createProveedor', [ControllerDistribuccion::class, 'createProveedor']);
    Route::post('crearVentaAuto', [ControllerDistribuccion::class, 'crearVentaAuto']);
    Route::post('createimpresora', [ControllerDistribuccion::class, 'createimpresora']);

    
    Route::post('descargaTotalBuseta', [ControllerDistribuccion::class, 'descargaTotalBuseta']);
    
    
Route::get('getProveedores/{idEmpresa}', [ControllerDistribuccion::class, 'getProveedores']);
Route::get('getimpresoras/{idEmpresa}', [ControllerDistribuccion::class, 'getimpresoras']);

Route::put('updateProveedor', [ControllerDistribuccion::class, 'updateProvedor']);

Route::put('updateCompra', [ControllerDistribuccion::class, 'updateCompra']);
Route::put('updateimpresora', [ControllerDistribuccion::class, 'updateimpresora']);



Route::post('createEmpleado', [ControllerDistribuccion::class, 'createEmpleado']);
Route::get('getEmpleados/{idEmpresa}', [ControllerDistribuccion::class, 'getEmpleados']);
Route::get('getEmpleadosVendedor/{idEmpresa}', [ControllerDistribuccion::class, 'getEmpleadosVendedor']);
Route::get('getEmpleadosChofer/{idEmpresa}', [ControllerDistribuccion::class, 'getEmpleadosChofer']);


Route::put('updateEmpleado', [ControllerDistribuccion::class, 'updateEmpleado']);
Route::get('getOrdenesPreventa/{idEmpresa}', [ControllerDistribuccion::class, 'getOrdenesPreventa']);

Route::get('getItmesOrdenes/{idOrden}', [ControllerDistribuccion::class, 'getItmesOrdenes']);


Route::post('createcliente', [ControllerDistribuccion::class, 'createcliente']);
Route::get('getclientes/{idEmpresa}', [ControllerDistribuccion::class, 'getclientes']);
Route::put('updateCliente', [ControllerDistribuccion::class, 'updateCliente']);
Route::get('getClienteForciNombre/{textoApp}/{idEmpresa}', [ControllerDistribuccion::class, 'getClienteForciNombre']);
Route::get('getProductosTexto/{textoApp}/{idEmpresa}', [ControllerDistribuccion::class, 'getProductosTexto']);
Route::put('crearCompra', [ControllerDistribuccion::class, 'crearCompra']);


Route::post('createRuta', [ControllerDistribuccion::class, 'createRuta']);
Route::get('getRutas/{idEmpresa}', [ControllerDistribuccion::class, 'getRutas']);
Route::put('updateRuta', [ControllerDistribuccion::class, 'updateRuta']);
Route::get('getClientesNoDesignados/{idEmpresa}', [ControllerDistribuccion::class, 'getClientesNoDesignados']);
Route::post('createRutaCliente', [ControllerDistribuccion::class, 'createRutaCliente']);
Route::get('getClientesRuta/{idRuta}', [ControllerDistribuccion::class, 'getClientesRuta']);
Route::put('deleteClienteRuta', [ControllerDistribuccion::class, 'deleteClienteRuta']);
Route::put('designarEmplieado', [ControllerDistribuccion::class, 'designarEmplieado']);


Route::post('createvehiculo', [ControllerDistribuccion::class, 'createvehiculo']);
Route::get('getvehiculos/{idEmpresa}', [ControllerDistribuccion::class, 'getvehiculos']);
Route::put('updatevehiculo', [ControllerDistribuccion::class, 'updatevehiculo']);
Route::put('designarEmpleadoAuto', [ControllerDistribuccion::class, 'designarEmpleadoAuto']);
Route::put('designarImpresora', [ControllerDistribuccion::class, 'designarImpresora']);
Route::post('updateProductosAuto', [ControllerDistribuccion::class, 'updateProductosAuto']);

Route::get('getAllProductosVehiculo/{idVehiculo}', [ControllerDistribuccion::class, 'getAllProductosVehiculo']);


Route::put('deleteProductoPromociosn', [ControllerDistribuccion::class, 'deleteProductoPromociosn']);


Route::get('getProductosIdPromocion/{idPromocion}', [ControllerDistribuccion::class, 'getProductosIdPromocion']);



Route::post('createProductoPromocion', [ControllerDistribuccion::class, 'createProductoPromocion']);


Route::post('nuevoProducto', [ControllerDistribuccion::class, 'nuevoProducto']);
Route::post('nuevoPromocion', [ControllerDistribuccion::class, 'nuevoPromocion']);
Route::put('updateProductoImagen', [ControllerDistribuccion::class, 'updateProductoImagen']);
Route::get('getProductosEmpresa/{idEmpresa}', [ControllerDistribuccion::class, 'getProductosEmpresa']);
Route::get('getPromocionesEmpresa/{idEmpresa}', [ControllerDistribuccion::class, 'getPromocionesEmpresa']);
Route::put('updateproductos', [ControllerDistribuccion::class, 'updateproductos']);

Route::post('itemProducto', [ControllerDistribuccion::class, 'itemProducto']);

Route::get('getKardexProducto/{idProducto}/{fechaInicio}/{fechaFin}', [ControllerDistribuccion::class, 'getKardexProducto']);

Route::get('getOrdenesFecha/{idEmpresa}/{fechaInicio}/{fechaFin}', [ControllerDistribuccion::class, 'getOrdenesFecha']);
Route::get('getOrdenesFechaCliente/{idEmpresa}/{fechaInicio}/{fechaFin}/{numeroCliente}', [ControllerDistribuccion::class, 'getOrdenesFechaCliente']);
Route::get('getOrdenesFechaVendedor/{idEmpresa}/{fechaInicio}/{fechaFin}/{numeroCliente}', [ControllerDistribuccion::class, 'getOrdenesFechaVendedor']);

Route::post('createOpciones', [ControllerDistribuccion::class, 'createOpciones']);
Route::get('allOpciones', [ControllerDistribuccion::class, 'allOpciones']);
Route::post('updateOpciones', [ControllerDistribuccion::class, 'updateOpciones']);
Route::post('createMenu', [ControllerDistribuccion::class, 'createMenu']);

Route::post('createmodulos', [ControllerDistribuccion::class, 'createmodulos']);

Route::post('activeOpcionesMenu', [ControllerDistribuccion::class, 'activeOpcionesMenu']);
Route::post('createRol', [ControllerDistribuccion::class, 'createRol']);
Route::post('activeRolMenu', [ControllerDistribuccion::class, 'activeRolMenu']);
Route::post('activeRolMenuOpcion', [ControllerDistribuccion::class, 'activeRolMenuOpcion']);
Route::post('createEmpresa', [ControllerDistribuccion::class, 'createEmpresa']);
Route::post('createUser', [ControllerDistribuccion::class, 'createUser']);

Route::post('activeRolUser', [ControllerDistribuccion::class, 'activeRolUser']);

Route::post('activeUserPermisos', [ControllerDistribuccion::class, 'activeUserPermisos']);

Route::post('activeUserEmpresa', [ControllerDistribuccion::class, 'activeUserEmpresa']);



Route::put('updateUsuario', [ControllerDistribuccion::class, 'updateUsuario']);
Route::put('updateEmpresa', [ControllerDistribuccion::class, 'updateEmpresa']);
Route::put('updateMenu', [ControllerDistribuccion::class, 'updateMenu']);
Route::put('updateModulos', [ControllerDistribuccion::class, 'updateModulos']);
Route::get('getAllModulos', [ControllerDistribuccion::class, 'getAllModulos']);
Route::get('getAllMenu', [ControllerDistribuccion::class, 'getAllMenu']);
Route::get('getListMenu/{idMenu}', [ControllerDistribuccion::class, 'getListMenu']);
Route::get('getAllRol', [ControllerDistribuccion::class, 'getAllRol']);
Route::get('getRolesMenu/{idRol}', [ControllerDistribuccion::class, 'getRolesMenu']);
Route::get('getListEmpresas', [ControllerDistribuccion::class, 'getListEmpresas']);

Route::get('getallUser', [ControllerDistribuccion::class, 'getallUser']);
Route::get('getUserRol/{idUser}', [ControllerDistribuccion::class, 'getUserRol']);

Route::get('getListUserEmpresas/{idUser}', [ControllerDistribuccion::class, 'getListUserEmpresas']);

Route::get('login/{usuario}/{pass}', [ControllerDistribuccion::class, 'login']);
Route::get('getListUserPermisos/{idUser}', [ControllerDistribuccion::class, 'getListUserPermisos']);
Route::put('updateRol', [ControllerDistribuccion::class, 'updateRol']);
});







