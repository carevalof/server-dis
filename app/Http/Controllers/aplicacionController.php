<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Eastwest\Json\Facades\Json;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class aplicacionController extends Controller
{

    public function getCargaUsuario($idUsuario,$idEmpresa)
    {
        $auto = DB::table('vehiculo')
            ->select('*')
            ->where('empleado', $idUsuario)
            ->where('empresa', $idEmpresa)
            ->first();

        $response = DB::table('veh_producto')
            ->join('producto', 'producto.pro_id', '=', 'veh_producto.producto')
            ->select('veh_producto.vep_cantidad','producto.pro_nombre','producto.pro_precio_pvp','producto.pro_precio_distribuccion','producto.pro_precio_tienda','producto.pro_id','producto.pro_tipo')
            ->where('veh_producto.vehiculo', $auto->veh_id)
            ->where('veh_producto.vep_estado', 1)
            ->where('veh_producto.vep_cantidad', '>', 0)
            ->get();
            

        $impresora = DB::table('impresora')
            ->select('*')
            ->where('imp_id', $auto->impresora)
            ->first();
        $objectResp = (object)['auto'=>$auto,'listaProductos'=>$response,'impresora'=>$impresora];

        return response()->json($objectResp);
    }

    public function updateRuta(Request $request)
    {

        $response = DB::table('ruta')
            ->select('*')
            ->where('ru_id', $request->ru_id)
            ->update([
                "ru_descripcion" => $request->ru_descripcion, 
                "ru_dia" => $request->ru_dia
             ]);

        return response()->json(["status" => true, "success" => true,"autenticado" => true, "data" => [],'message' => 'Empleado Asignado']);

   
    }

    public function createRuta(Request $request)
    {
        $empresa = DB::table('ruta')->insert([
            "ru_descripcion" => $request->ru_descripcion,
            "ru_estado" => 1, 
            "ru_dia" => $request->ru_dia, 
            "empresa" => $request->empresa, 
            "empleado" => $request->empleado, 
            "chofer" => 0, 
        ]);

        return response()->json(["status" => true, "success" => true,"autenticado" => true, "data" => $objectResp,'message' => 'Logeo Correcto']);

    }



    public function updateRutaCliente(Request $request)
    {
        
        DB::table('ruta_cliente')
                    ->select('*')
                    ->where('cliente', $request->cliente)
                    ->where('ruta', $request->ruta)
                    ->update([
                        "ru_cli_estado" => 0, 
                            ]);

        return response()->json(["status" => true, "success" => true,"autenticado" => true, "data" => [],'message' => 'Cliente eliminado Correcto']);

    }




    public function createRutaClientes(Request $request)
    {



        $listaClientes =$request->input('listaClientes');
        $listaClientesAux = json_decode($listaClientes, true);

        foreach ($listaClientesAux as $cliente) {

            $response = DB::table('ruta_cliente')
                ->select('*')
                ->where('cliente', $cliente['cli_id'])
                ->where('ruta', $request->ruta)
                ->first();

            if($response == NULL){

                DB::table('ruta_cliente')->insert([
                    "cliente" => $cliente['cli_id'],
                    "ruta" => $request->ruta, 
                    "ru_cli_estado" => 1
                ]);

            }else{


                DB::table('ruta_cliente')
                    ->select('*')
                    ->where('cliente', $cliente['cli_id'])
                    ->update([
                        "ru_cli_estado" => 0, 
                            ]);



                DB::table('ruta_cliente')
                    ->select('*')
                    ->where('cliente', $cliente['cli_id'])
                    ->where('ruta', $request->ruta)
                    ->update([
                        "ru_cli_estado" => 1, 
                            ]);
            }     


        }
           


        return response()->json(["status" => true, "success" => true,"autenticado" => true, "data" => [],'message' => 'Clientes Ingresados Correctamente']);

        

    }

    public function getRutasEmpleado($idEmpresa,$idEmpleado){

        $allRutas = DB::table('ruta')
            ->select('*')
            ->where('ru_estado',1)
            ->where('empresa',$idEmpresa)
            ->where('empleado',$idEmpleado)
            ->get();

    

        return response()->json($allRutas);

    }


    public function getRutas($idEmpresa){

        $allRutas = DB::table('ruta')
            ->select('*')
            ->where('ru_estado',1)
            ->where('empresa',$idEmpresa)
            ->get();

       

        $listaEmpleadoRuta = [];

        foreach ($allRutas as $ruta) {

            if($ruta->empleado == 0){

                array_push($listaEmpleadoRuta, (object)['ru_id'=>$ruta->ru_id,'ru_descripcion'=>$ruta->ru_descripcion,'ru_dia'=> $ruta->ru_dia,'empleado'=>"No Designado"]);


            }else{

                $empleado = DB::table('empleado')
                    ->select('*')
                    ->where('emp_id', $ruta->empleado)
                    ->first();

                array_push($listaEmpleadoRuta, (object)['ru_id'=>$ruta->ru_id,'ru_descripcion'=>$ruta->ru_descripcion,'ru_dia'=> $ruta->ru_dia,'empleado'=>$empleado->emp_nombre." ".$empleado->emp_apellido]);


            }

        }

        return response()->json($listaEmpleadoRuta);

    }

    public function getclientes($idEmpresa,$latitud,$longitud){

     

        $results = DB::select( DB::raw("SELECT *, (6371 * ACOS( 
            SIN(RADIANS(cli_latitud)) * SIN(RADIANS('$latitud')) 
            + COS(RADIANS(cli_longitud - '$longitud')) * COS(RADIANS(cli_latitud)) 
            * COS(RADIANS('$latitud'))
            )
            ) AS distance
            FROM cliente left join ruta_cliente on ruta_cliente.cliente = cliente.cli_id left join ruta on ruta_cliente.ruta = ruta.ru_id left join empleado on ruta.empleado = empleado.emp_id 
            Where cliente.cli_empresa_id = '$idEmpresa' AND ruta_cliente.ru_cli_estado = 0 OR ruta_cliente.ru_cli_estado  IS NULL
            HAVING distance < 1 
            ORDER BY distance ASC") );


        return response()->json($results);
    }

    public function getProductoStock($idEmpresa)
    {
        $response = DB::table('producto')
            ->select('*')
            ->where('pro_empresa_distribuidora', $idEmpresa)
            ->where('pro_stop', '>',0)
            ->get();

        return response()->json($response);
    }

    public function getProducto($idEmpresa)
    {
        $response = DB::table('producto')
            ->select('*')
            ->where('pro_empresa_distribuidora', $idEmpresa)
            ->where('pro_stop', '>',0)
            ->get();

        $responseClientes = DB::table('cliente')
            ->select('*')
            ->where('cli_empresa_id', $idEmpresa)
            ->where('cli_ci', '<>',"")
            ->where('cli_id', '<>',2)
            ->get();

        $objectResp = (object)['listaProductos'=>$response,'clientes'=>$responseClientes];

        return response()->json($objectResp);
    }


    public function getClientesRutas($idRuta)
    {
       
       
        $response = DB::table('ruta_cliente')
            ->join('cliente', 'cliente.cli_id', '=', 'ruta_cliente.cliente')
            ->select('*')
            ->where('ruta_cliente.ru_cli_estado', 1)
            ->where('ruta_cliente.ruta', $idRuta)
            ->get();

            
        return response()->json($response);
    }


    public function loginApp($usuario,$pass){


        $responseperson = DB::table('empleado')
            ->select('*')
            ->where('emp_usuario',$usuario)
            ->where('emp_estado',1)
            ->first();

        if($responseperson !== NULL){

            if($responseperson->emp_contrasena == $pass){


                $empresa = DB::table('empresa1')
                        ->select('*')
                        ->where('EMP_ESTADO',1)
                        ->where('EMP_CODIGO',$responseperson->emp_usuario_empresa_id)
                        ->first();


                $objectResp = (object)['empleado'=>$responseperson,'empresa'=>$empresa];

                return response()->json(["status" => true, "success" => true,"autenticado" => true, "data" => $objectResp,'message' => 'Logeo Correcto']);

                

            }else{

                return response()->json(["status" => true, "success" => false,"data" => 'Contraseña Incorrecta','message' => 'Contraseña Incorrecta']);

            }

        }else{

            return response()->json(["status" => true, "success" => false,"data" => 'Usuario no existe comuniquese con el administrador','message' => 'Usuario no existe comuniquese con el administrador']);

        }




    }

    public function crearVentaAuto(Request $request)
    {
        $vehiculo = DB::table('vehiculo')
            ->select('*')
            ->where('veh_id',$request->idVehiculo)
            ->first();

        $autoventa =  DB::table('autoventa')->insertGetId([
                    "aven_fecha" => $request->aven_fecha, 
                    "empleado" =>$vehiculo->empleado,
                    "aven_descripcion" =>$request->aven_descripcion,
                ]);


        $listaor =$request->input('listaordenes');
        $listaordenes = json_decode($listaor, true);

        foreach ($listaordenes as $ordenes) {

            $factura = DB::table('compra')->insertGetId([
                        "com_cliente" => $ordenes['com_cliente'], 
                        "com_fecha" => $ordenes['com_fecha'],
                        "com_latitud" =>$ordenes['com_latitud'], 
                        "com_longitud"=>$ordenes['com_longitud'],
                        "com_descuento" => $ordenes['com_descuento'], 
                        "com_sub_total" => $ordenes['com_sub_total'], 
                        "com_total" => $ordenes['com_total'], 
                        "com_iva" => $ordenes['com_iva'], 
                        "empleado_id" => $vehiculo->empleado , 
                        "com_tipo" => $ordenes['com_tipo'], 
                        "emp_id" => $ordenes['emp_id'], 
                        "autoventa" => $autoventa 
                    ]);


                    $visita = DB::table('visita')->insert([
                        "vis_fecha" => $ordenes['com_fecha'],
                        "vis_empleado" => $vehiculo->empleado, 
                        "vis_empresa" => $ordenes['emp_id'], 
                        "vis_latittud" => $ordenes['com_latitud'], 
                        "vis_longitud" => $ordenes['com_longitud'], 
                        "vis_tipo" => 2, 
                        "vis_cliente" => $ordenes['com_cliente'], 
                       
                    ]);

                
            
                // $listaor =$request->input('lista_detalle');
                    $lista = $ordenes['lista_detalle'];
            
                    foreach ($lista as $orden) {
            
                        $empresa = DB::table('detalle_compra')->insert([
                            "dcom_producto" => $orden['pro_id'],
                            "dcom_compra" => $factura,
                            "dcom_cantidad" => (int)$orden['pro_cantidad'],
                            "dcom_precio_unitario" => (float)$orden['unitario'],
                            "dcom_precio_total" => (float)$orden['total'],
                        ]);

                        $responsea = DB::table('veh_producto')
                            ->select('*')
                            ->where('vehiculo', $request->idVehiculo)
                            ->where('producto', $orden['pro_id'])
                            ->first();

                        if($responsea == NULL){}
                        else{

                            $newCantidad = $responsea->vep_cantidad - (int)$orden['pro_cantidad'];

                            DB::table('veh_producto')
                                ->select('*')
                                ->where('vehiculo', $request->idVehiculo)
                                ->where('producto', $orden['pro_id'])
                                ->update([
                                    "vep_cantidad" => $newCantidad, 
                                        ]);


                        }


        }

    

        
        }


        return response()->json(["status" => true, "success" => true,"data" => 'Ingreso de datos','message' => 'Ingreso de datos correctos']);


    }


    public function crearProductosVentaAuto(Request $request)
    {
        $vehiculo = DB::table('vehiculo')
            ->select('*')
            ->where('veh_id',$request->idVehiculo)
            ->first();

        $autoventa =  DB::table('autoventa')->insertGetId([
                    "aven_fecha" => $request->aven_fecha, 
                    "empleado" =>$vehiculo->empleado,
                    "aven_descripcion" =>$request->aven_descripcion,
                ]);


        $listaor =$request->input('listaordenes');
        $listaordenes = json_decode($listaor, true);

        foreach ($listaordenes as $ordenes) {

            $factura = DB::table('compra')->insertGetId([
                        "com_cliente" => $ordenes['com_cliente'], 
                        "com_fecha" => $ordenes['com_fecha'],
                        "com_descuento" => $ordenes['com_descuento'], 
                        "com_sub_total" => $ordenes['com_sub_total'], 
                        "com_total" => $ordenes['com_total'], 
                        "com_iva" => $ordenes['com_iva'], 
                        "empleado_id" => $vehiculo->empleado , 
                        "com_tipo" => $ordenes['com_tipo'], 
                        "emp_id" => $ordenes['emp_id'], 
                        "autoventa" => $autoventa 
                    ]);
            
                // $listaor =$request->input('lista_detalle');
                    $lista = $ordenes['lista_detalle'];
            
                    foreach ($lista as $orden) {
            
                        $empresa = DB::table('detalle_compra')->insert([
                            "dcom_producto" => $orden['pro_id'],
                            "dcom_compra" => $factura,
                            "dcom_cantidad" => (int)$orden['pro_cantidad'],
                            "dcom_precio_unitario" => (float)$orden['unitario'],
                            "dcom_precio_total" => (float)$orden['total'],
                        ]);

                        $responsea = DB::table('veh_producto')
                            ->select('*')
                            ->where('vehiculo', $request->idVehiculo)
                            ->where('producto', $orden['pro_id'])
                            ->first();

                        if($responsea == NULL){}
                        else{

                            $newCantidad = $responsea->vep_cantidad - (int)$orden['pro_cantidad'];

                            DB::table('veh_producto')
                                ->select('*')
                                ->where('vehiculo', $request->idVehiculo)
                                ->where('producto', $orden['pro_id'])
                                ->update([
                                    "vep_cantidad" => $newCantidad, 
                                        ]);


                        }


        }

    

        
        }


        return response()->json(["status" => true, "success" => true,"data" => 'Ingreso de datos','message' => 'Ingreso de datos correctos']);


    }

    public function createcliente(Request $request)
    {

        if(DB::table('cliente')
                    ->where('cli_ci',$request->cli_ci)
                    ->exists())
        {

            return response()->json(["status" => true, "success" => false,"data" => "",'message' => 'Cliente ya existe']);


        }else{

            $empresa = DB::table('cliente')->insert([
                "cli_nombre" => $request->cli_nombre, 
                "cli_ci" => $request->cli_ci, 
                "cli_apellido" => $request->cli_apellido, 
                "cli_telefono" => $request->cli_telefono, 
                "cli_direccion" => $request->cli_direccion, 
                "cli_latitud" => $request->cli_latitud, 
                "cli_email" => $request->cli_email, 
                "cli_longitud" => $request->cli_longitud, 
                "cli_empresa_id" => $request->cli_empresa_id, 
            ]);
            return response()->json(["status" => true, "success" => true,"data" => $empresa,'message' => 'Ingreso Correcto']);
        }

       

    }

    public function createclienteVisita(Request $request)
    {

        if(DB::table('cliente')
                    ->where('cli_ci',$request->cli_ci)
                    ->exists())
        {

            return response()->json(["status" => true, "success" => false,"data" => "",'message' => 'Cliente ya existe']);


        }else{

            $empresa = DB::table('cliente')->insertGetId([
                "cli_nombre" => $request->cli_nombre, 
                "cli_ci" => $request->cli_ci, 
                "cli_apellido" => $request->cli_apellido, 
                "cli_telefono" => $request->cli_telefono, 
                "cli_direccion" => $request->cli_direccion, 
                "cli_latitud" => $request->cli_latitud, 
                "cli_email" => $request->cli_email, 
                "cli_longitud" => $request->cli_longitud, 
                "cli_empresa_id" => $request->cli_empresa_id,
                "cli_nombre_local"=>  $request->cli_nombre_local
            ]);

            $response = DB::table('ruta_cliente')
                ->select('*')
                ->where('cliente', $empresa)
                ->where('ruta', $request->ruta)
                ->first();

            if($response == NULL){

                DB::table('ruta_cliente')->insert([
                    "cliente" => $empresa,
                    "ruta" => $request->ruta, 
                    "ru_cli_estado" => 1
                ]);

            }else{


                DB::table('ruta_cliente')
                    ->select('*')
                    ->where('cliente', $empresa)
                    ->update([
                        "ru_cli_estado" => 0, 
                            ]);



                DB::table('ruta_cliente')
                    ->select('*')
                    ->where('cliente', $empresa)
                    ->where('ruta', $request->ruta)
                    ->update([
                        "ru_cli_estado" => 1, 
                            ]);
            }     

            return response()->json(["status" => true, "success" => true,"data" => $empresa,'message' => 'Ingreso Correcto']);
        }

       

    }


    public function updatecliente(Request $request)
    {

        


        DB::table('cliente')
                ->select('*')
                ->where('cli_id', $request->cli_id)
                ->update([
                    "cli_nombre" => $request->cli_nombre, 
                "cli_ci" => $request->cli_ci, 
                "cli_apellido" => $request->cli_apellido, 
                "cli_telefono" => $request->cli_telefono, 
                "cli_direccion" => $request->cli_direccion, 
                "cli_email" => $request->cli_email, 
                "cli_nombre_local" =>$request->cli_nombre_local, 
                        ]);

       
            return response()->json(["status" => true, "success" => true,"data" => [],'message' => 'actualizacion Correcto']);
        

       

    }

    public function getimpresoras($idEmpresa){

        $allEmpleados = DB::table('impresora')
            ->select('*')
            ->where('empresa',$idEmpresa)
            ->get();

        return response()->json($allEmpleados);
    }

    public function getAllProductosVehiculo($idVehiculo)
    {
        
        $response = DB::table('veh_producto')
            ->join('producto', 'producto.pro_id', '=', 'veh_producto.producto')
            ->select('*')
            ->where('veh_producto.vehiculo', $idVehiculo)
            ->where('veh_producto.vep_estado', 1)
            ->where('producto.pro_stop', '>', 0)
            ->get();

            $listaProductos = [];

            foreach ($response as $ruta) {

    
                    array_push($listaProductos, (object)['vep_cantidad'=>$ruta->vep_cantidad,'pro_stop'=>$ruta->pro_stop,'pro_nombre'=> $ruta->pro_nombre,'pro_id'=> $ruta->pro_id,'cantidad_lista'=>0]);
            }
    
            

        return response()->json($listaProductos);
    }

    public function getvehiculos($idEmpresa){

        $allRutas = DB::table('vehiculo')
            ->select('*')
            ->where('veh_estado',1)
            ->where('empresa',$idEmpresa)
            ->get();

       

        $listaEmpleadoRuta = [];

        foreach ($allRutas as $ruta) {

            if($ruta->empleado == 0){

                array_push($listaEmpleadoRuta, (object)['veh_id'=>$ruta->veh_id,'veh_placa'=>$ruta->veh_placa,'veh_anio'=> $ruta->veh_anio,'veh_color'=> $ruta->veh_color,'veh_marca'=> $ruta->veh_marca,'empleado'=>"No Designado",'id_empleado'=>0]);


            }else{

                $empleado = DB::table('empleado')
                    ->select('*')
                    ->where('emp_id', $ruta->empleado)
                    ->first();

                array_push($listaEmpleadoRuta, (object)['veh_id'=>$ruta->veh_id,'veh_placa'=>$ruta->veh_placa,'veh_anio'=> $ruta->veh_anio,'veh_color'=> $ruta->veh_color,'veh_marca'=> $ruta->veh_marca,'empleado'=>$empleado->emp_nombre." ".$empleado->emp_apellido,'id_empleado'=>$empleado->emp_id]);


            }

        }

        return response()->json($listaEmpleadoRuta);

    }


    public function getOrdenesFechaEmpleado($idEmpresa,$idEmpleado,$fechaInicio, $fechaFin){

        $allPreventa = DB::table('compra')
            ->join('empleado','compra.empleado_id', '=', 'empleado.emp_id')
            ->join('cliente','compra.com_cliente', '=', 'cliente.cli_id')
            ->select('*')
            ->where('compra.emp_id',$idEmpresa)
            ->where('compra.empleado_id',$idEmpleado)
            ->whereBetween('compra.com_fecha', [$fechaInicio, $fechaFin])
            ->get();

        return response()->json($allPreventa);

    }


    public function getListaVisitaFecha($idEmpresa,$fecha,$fechafin){

        $allVisitas = DB::table('visita')
            ->join('empleado','visita.vis_empleado', '=', 'empleado.emp_id')
            ->join('cliente','visita.vis_cliente', '=', 'cliente.cli_id')
            ->select('*')
            ->where('visita.vis_empresa',$idEmpresa)
            ->whereBetween('visita.vis_fecha', [$fecha, $fechafin])
            ->get();

        return response()->json($allVisitas);

    }

    public function getEmpleados($idEmpresa){

        $allEmpleados = DB::table('empleado')
            ->select('*')
            ->where('emp_usuario_empresa_id',$idEmpresa)
            ->get();
        return response()->json($allEmpleados);
    }

    public function designarEmplieado(Request $request)
    {

        if($request->tipo == 1){
            DB::table('ruta')
                ->select('*')
                ->where('ru_id', $request->ruta)
                ->update([
                    "empleado" => $request->empleado, 
                        ]);

        }

        else if($request->tipo == 2){

            DB::table('ruta')
                ->select('*')
                ->where('ru_id', $request->ruta)
                ->update([
                    "chofer" => $request->empleado, 
                        ]);

        }
        

        return response()->json(["status" => true, "success" => true,"autenticado" => true, "data" => [],'message' => 'Empleado Asignado']);

    }

    public function getOrdenesFecha($idEmpresa,$tipo,$fechaInicio, $fechaFin){

        $allPreventa = DB::table('compra')
            ->join('empleado','compra.empleado_id', '=', 'empleado.emp_id')
            ->join('cliente','compra.com_cliente', '=', 'cliente.cli_id')
            ->select('*')
            ->where('compra.emp_id',$idEmpresa)
            ->where('compra.com_tipo',$tipo)
            ->whereBetween('compra.com_fecha', [$fechaInicio, $fechaFin])
            ->get();

        return response()->json($allPreventa);

    }

    public function getItmesOrdenes($idOrden){

        $allPreventa = DB::table('detalle_compra')
            ->join('producto','detalle_compra.dcom_producto', '=', 'producto.pro_id')
            ->select('*')
            ->where('dcom_compra',$idOrden)
            ->get();

        return response()->json($allPreventa);

    }

    public function updateProductosAuto(Request $request)
    {

        $listaProductos =$request->input('listaPro');
        $listaPro = json_decode($listaProductos, true);



        foreach ($listaPro as $pro) {


            $responsea = DB::table('veh_producto')
                    ->select('*')
                    ->where('vehiculo', $request->vehiculo)
                    ->where('producto', $pro['pro_id'])
                    ->first();

            if($responsea == NULL){

                DB::table('veh_producto')->insert([
                    "vehiculo" => $request->vehiculo,
                    "producto" => $pro['pro_id'], 
                    "vep_estado" => 1,
                    "vep_cantidad"=>$pro['cantidad_lista']
                ]);
            }else{

                $newCantidad = $pro['cantidad_lista'] + $responsea->vep_cantidad;

                DB::table('veh_producto')
                    ->select('*')
                    ->where('vehiculo', $request->vehiculo)
                    ->where('producto', $pro['pro_id'])
                    ->update([
                        "vep_cantidad" => $newCantidad, 
                            ]);

            }

            $kardex = DB::table('kardex')->insert([
                "ka_producto" => $pro['pro_id'], 
                "ka_tipo" => 2, 
                "ka_cantidad" => $pro['cantidad_lista'], 
                "ka_fecha_ingreso" => now(), 
                "ka_descripcion" => "Carga a buseta",
            ]);

            $response = DB::table('producto')
                ->select('*')
                ->where('pro_id', $pro['pro_id'])
                ->first();
                
            $cantidad = $response->pro_stop;

            $cantidad = $cantidad - $pro['cantidad_lista'];

            if($cantidad < 0){
                $cantidad = 0;
            }
            

            $responseProducto = DB::table('producto')
            ->select('*')
            ->where('pro_id', $pro['pro_id'])
            ->update([
                "pro_stop" => $cantidad, 
                    ]);

        }

      
        
        return response()->json(["status" => true, "success" => true,"data" => 'Ingreso de datos','message' => 'Ingreso de datos correctos']);


    
    }


    public function updateCompra(Request $request)
    {



        $response = DB::table('compra')
            ->select('*')
            ->where('com_id', $request->com_id)
            ->update([
                        "com_tipo" => $request->com_tipo, 
                    ]);

        if( $request->com_tipo == 4){

                        $allProductos = DB::table('detalle_compra')
                           ->select('*')
                           ->where('dcom_compra',$request->com_id)
                           ->get();
           
                           

                           //andres
                           foreach ($allProductos as $pro) {
           
                            
        
                               $kardex = DB::table('kardex')->insert([
                                   "ka_producto" => $pro->dcom_producto, 
                                   "ka_tipo" => 1, 
                                   "ka_cantidad" =>  $pro->dcom_cantidad, 
                                   "ka_descripcion" => "Venta Cancelada",
                                   "ka_fecha_ingreso" => now(), 
                                   ]);
                                   error_log("aaaaaaa");
                                   error_log($pro->dcom_producto);
                                   error_log("dcom_producto");
                                //    error_log($pro['dcom_producto']);
                                  
                               $idProducto  = $pro->dcom_producto;
                               $producto = DB::table('producto')
                                   ->select('*')
                                   ->where('pro_id',$idProducto)
                                   ->first();

                                   
                                 
                                           
                               $cantidad = $producto->pro_stop;
                               
                               $cantidad = $cantidad + $pro->dcom_cantidad;
                                       
                               
                               $responseProducto = DB::table('producto')
                                   ->select('*')
                                   ->where('pro_id', $idProducto)
                                   ->update([
                                           "pro_stop" => $cantidad, 
                                           ]);
                               }
           
           
                                   
                   }


        return response()->json(["status" => true, "success" => true,"data" => 'Actulizado','message' => 'Actulizado']);

    }

    public function crearVisita(Request $request)
    {

        //andres
        $visita = DB::table('visita')->insert([
            "vis_fecha" => $request->vis_fecha,
            "vis_empleado" => $request->vis_empleado, 
            "vis_empresa" => $request->vis_empresa, 
            "vis_latittud" => $request->vis_latittud, 
            "vis_longitud" => $request->vis_longitud, 
            "vis_tipo" => $request->vis_tipo, 
            "vis_cliente" => $request->vis_cliente, 
           
        ]);

        return response()->json(["status" => true, "success" => true, "data" => $visita,'message' => 'Visita Correcta']);

    }

    public function crearCompra(Request $request)
    {

        $listaor1 =$request->input('lista_detalle');
        $lista1 = json_decode($listaor1, true);

        $listaProductos = [];
        foreach ($lista1 as $orden) {
            $response = DB::table('producto')
                        ->select('*')
                        ->where('pro_id', $orden['pro_id'])
                        ->first();
            
            if($response->pro_stop >= (int)$orden['pro_cantidad']){
            }else{
                array_push($listaProductos, $response);
            }            
            
        }


        if(count($listaProductos) > 0){

            return response()->json(["status" => true, "success" => false, "message" => "No existe producto","listaProductos" => $listaProductos]);

        }else{
    
        if($request->tipo_compra == 1){

            $factura = DB::table('compra')->insertGetId([
                "com_cliente" => $request->com_cliente, 
                "com_fecha" => $request->com_fecha, 
                "com_descuento" => $request->com_descuento, 
                "com_sub_total" => $request->com_sub_total, 
                "com_total" => $request->com_total, 
                "com_iva" => $request->com_iva, 
                "com_latitud" =>$request->com_latitud, 
                "com_longitud"=>$request->com_longitud,
                "empleado_id" => $request->empleado_id, 
                "com_tipo" => $request->com_tipo, 
                "com_tipo_impresion"=>$request->com_tipo_impresion, 
                "emp_id" => $request->emp_id, 
            ]);

            $visita = DB::table('visita')->insert([
                "vis_fecha" => $request->com_fecha,
                "vis_empleado" => $request->empleado_id, 
                "vis_empresa" => $request->emp_id, 
                "vis_latittud" => $request->com_latitud, 
                "vis_longitud" => $request->com_longitud, 
                "vis_tipo" => 2, 
                "vis_cliente" => $request->com_cliente, 
               
            ]);

      
    
            $listaor =$request->input('lista_detalle');
            $lista = json_decode($listaor, true);
    
            foreach ($lista as $orden) {
    
                $empresa = DB::table('detalle_compra')->insert([
                    "dcom_producto" => $orden['pro_id'],
                    "dcom_compra" => $factura,
                    "dcom_cantidad" => (int)$orden['pro_cantidad'],
                    "dcom_precio_unitario" => (float)$orden['unitario'],
                    "dcom_precio_total" => (float)$orden['total'],
                ]);


                if($orden['pro_tipo'] == 1){

                    $kardex = DB::table('kardex')->insert([
                        "ka_producto" => $orden['pro_id'], 
                        "ka_tipo" => 2, 
                        "ka_cantidad" => (int)$orden['pro_cantidad'], 
                        "ka_fecha_ingreso" => $request->com_fecha, 
                        "ka_descripcion" => "Compra mediante Web"
                    ]);
            
                    $response = DB::table('producto')
                        ->select('*')
                        ->where('pro_id', $orden['pro_id'])
                        ->first();
                        
                    $cantidad = $response->pro_stop;
            
                    $cantidad = $cantidad - (int)$orden['pro_cantidad'];
        
                    if($cantidad < 0){
                        $cantidad = 0;
                    }
                    
            
                    $responseProducto = DB::table('producto')
                    ->select('*')
                    ->where('pro_id', $orden['pro_id'])
                    ->update([
                        "pro_stop" => $cantidad, 
                            ]);

                }

                else if($orden['pro_tipo'] == 2){


                    $productosPromocion = DB::table('promocion_producto')
                        ->select('*')
                        ->where('promocion',$orden['pro_id'])
                        ->where('prp_estado',1)
                        ->get();

                    foreach ($productosPromocion as $producto) {

                        $kardex = DB::table('kardex')->insert([
                            "ka_producto" => $producto->producto, 
                            "ka_tipo" => 2, 
                            "ka_cantidad" => ((int)$producto->prp_cantidad * (int)$orden['pro_cantidad']), 
                            "ka_fecha_ingreso" => $request->com_fecha, 
                            "ka_descripcion" => "Compra mediante Web"
                        ]);
                
                        $response = DB::table('producto')
                            ->select('*')
                            ->where('pro_id', $producto->producto)
                            ->first();
                            
                        $cantidad = $response->pro_stop;
                
                        $cantidad = $cantidad - ((int)$producto->prp_cantidad * (int)$orden['pro_cantidad']);
            
                        if($cantidad < 0){
                            $cantidad = 0;
                        }
                        
                
                        $responseProducto = DB::table('producto')
                        ->select('*')
                        ->where('pro_id', $producto->producto)
                        ->update([
                            "pro_stop" => $cantidad, 
                                ]);


                        }

                }
    
                
               
            }

        
        
    
            return response()->json(["status" => true, "success" => true, "message" => "Pedido exitoso","listaProductos" => $listaProductos]);
        
        }
    }


    }


    //
}
