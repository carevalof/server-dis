<?php

namespace App\Http\Controllers;
 
use JWTAuth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Eastwest\Json\Facades\Json;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Log;


class ControllerDistribuccion extends Controller
{
    
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login','register']]);
    }

    public function guard()
    {
        return Auth::guard();
    }
    
    public function createProveedor(Request $request)
    {

        $empresa = DB::table('empresa')->insert([
            "emp_estado" => 1,
            "emp_nombre" => $request->emp_nombre, 
            "emp_usuario_empresa_id" => $request->emp_usuario_empresa_id, 
        ]);

    }

    public function createimpresora(Request $request)
    {
        $empresa = DB::table('impresora')->insert([
            "imp_nombre" => $request->imp_nombre, 
            "imp_mac" => $request->imp_mac, 
            "empresa" =>$request->empresa
        ]);

    }

    public function createRuta(Request $request)
    {
        $empresa = DB::table('ruta')->insert([
            "ru_descripcion" => $request->ru_descripcion,
            "ru_estado" => 1, 
            "ru_dia" => $request->ru_dia, 
            "empresa" => $request->empresa, 
            "empleado" => $request->empleado, 
            "chofer" => 0, 
        ]);

    }


    public function createvehiculo(Request $request)
    {
        $vehiculo = DB::table('vehiculo')->insert([
            "veh_placa" => $request->veh_placa,
            "veh_estado" => 1, 
            "veh_anio" => $request->veh_anio, 
            "veh_color" => $request->veh_color, 
            "veh_marca" => $request->veh_marca, 
            "empresa" => $request->empresa, 
            "empleado" => $request->empleado, 
            "impresora" => 0
      
        ]);

    }

    
    public function updateProductosAuto(Request $request)
    {

        if($request->tipo == 1){

                $responsea = DB::table('veh_producto')
                    ->select('*')
                    ->where('vehiculo', $request->vehiculo)
                    ->where('producto', $request->producto)
                    ->first();

            if($responsea == NULL){

                DB::table('veh_producto')->insert([
                    "vehiculo" => $request->vehiculo,
                    "producto" => $request->producto, 
                    "vep_estado" => 1,
                    "vep_cantidad"=>$request->vep_cantidad
                ]);
            }else{

                $newCantidad = $request->vep_cantidad + $responsea->vep_cantidad;

                DB::table('veh_producto')
                    ->select('*')
                    ->where('vehiculo', $request->vehiculo)
                    ->where('producto', $request->producto)
                    ->update([
                        "vep_cantidad" => $newCantidad, 
                            ]);

            }

            $kardex = DB::table('kardex')->insert([
                "ka_producto" => $request->producto, 
                "ka_tipo" => 2, 
                "ka_cantidad" => $request->vep_cantidad, 
                "ka_fecha_ingreso" => now(), 
                "ka_descripcion" => "Carga a buseta",
            ]);

            $response = DB::table('producto')
                ->select('*')
                ->where('pro_id', $request->producto)
                ->first();
                
            $cantidad = $response->pro_stop;

            $cantidad = $cantidad - $request->vep_cantidad;

            if($cantidad < 0){
                $cantidad = 0;
            }
            

            $responseProducto = DB::table('producto')
            ->select('*')
            ->where('pro_id', $request->producto)
            ->update([
                "pro_stop" => $cantidad, 
                    ]);


        }

        else if($request->tipo == 2){

            $responsea = DB::table('veh_producto')
                    ->select('*')
                    ->where('vehiculo', $request->vehiculo)
                    ->where('producto', $request->producto)
                    ->first();

            if($responsea == NULL){}
            else{

                $newCantidad = $responsea->vep_cantidad - $request->vep_cantidad;

                DB::table('veh_producto')
                    ->select('*')
                    ->where('vehiculo', $request->vehiculo)
                    ->where('producto', $request->producto)
                    ->update([
                        "vep_cantidad" => $newCantidad, 
                            ]);


            }


            $kardex = DB::table('kardex')->insert([
                "ka_producto" => $request->producto, 
                "ka_tipo" => 1, 
                "ka_cantidad" => $request->vep_cantidad, 
                "ka_fecha_ingreso" => now(), 
                "ka_descripcion" => "Descarga de buseta",
            ]);

            $response = DB::table('producto')
                ->select('*')
                ->where('pro_id', $request->producto)
                ->first();
                
            $cantidad = $response->pro_stop;

            $cantidad = $cantidad + $request->vep_cantidad;

           
        
            $responseProducto = DB::table('producto')
            ->select('*')
            ->where('pro_id', $request->producto)
            ->update([
                "pro_stop" => $cantidad, 
                    ]);




        }
        
    }

    public function descargaTotalBuseta(Request $request)
    {

        $allProductosAuto = DB::table('veh_producto')
                    ->select('*')
                    ->where('vehiculo', $request->vehiculo)
                    ->where('vep_cantidad', '>', 0)
                    ->get();


        foreach ($allProductosAuto as $item) {  


            $responsea = DB::table('veh_producto')
                    ->select('*')
                    ->where('vehiculo', $request->vehiculo)
                    ->where('producto', $item->producto)
                    ->first();

            if($responsea == NULL){}
            else{

                $newCantidad = $responsea->vep_cantidad - $item->vep_cantidad;

                DB::table('veh_producto')
                    ->select('*')
                    ->where('vehiculo', $request->vehiculo)
                    ->where('producto', $item->producto)
                    ->update([
                        "vep_cantidad" => $newCantidad, 
                            ]);


            }


            $kardex = DB::table('kardex')->insert([
                "ka_producto" => $item->producto, 
                "ka_tipo" => 1, 
                "ka_cantidad" => $item->vep_cantidad, 
                "ka_fecha_ingreso" => now(), 
                "ka_descripcion" => "Descarga de buseta",
            ]);

            $response = DB::table('producto')
                ->select('*')
                ->where('pro_id', $item->producto)
                ->first();
                
            $cantidad = $response->pro_stop;

            $cantidad = $cantidad + $item->vep_cantidad;

           
        
            $responseProducto = DB::table('producto')
            ->select('*')
            ->where('pro_id', $item->producto)
            ->update([
                "pro_stop" => $cantidad, 
                    ]);




        }         

      
        
    }


    public function createRutaCliente(Request $request)
    {

        
        
        $response = DB::table('ruta_cliente')
                ->select('*')
                ->where('cliente', $request->cliente)
                ->where('ruta', $request->ruta)
                ->first();

        if($response == NULL){

            DB::table('ruta_cliente')->insert([
                "cliente" => $request->cliente,
                "ruta" => $request->ruta, 
                "ru_cli_estado" => 1
            ]);

        }else{

            DB::table('ruta_cliente')
                ->select('*')
                ->where('cliente', $request->cliente)
                ->where('ruta', $request->ruta)
                ->update([
                    "ru_cli_estado" => 1, 
                        ]);
        }        


        

    }



    public function createProductoPromocion(Request $request)
    {

     
        $response = DB::table('promocion_producto')
                ->select('*')
                ->where('promocion', $request->promocion)
                ->where('producto', $request->producto)
                ->first();

        if($response == NULL){

            DB::table('promocion_producto')->insert([
                "promocion" => $request->promocion,
                "producto" => $request->producto,
                "prp_cantidad" => $request->prp_cantidad, 
                "prp_estado" => 1
            ]);

        }else{

            DB::table('promocion_producto')
                ->select('*')
                ->where('promocion', $request->promocion)
                ->where('producto', $request->producto)
                ->update([
                    "prp_estado" => 1, 
                    "prp_cantidad" => $request->prp_cantidad, 
                        ]);
        }        


        

    }


    public function getUserRol($idUser)
    {
        $response = DB::table('roles')
            ->select('ROL_CODIGO','ROL_NOMBRE','ROL_DESCRIPCION','ROL_ESTADO')
            ->where('ROL_ESTADO', 1)
            ->get();


        $responsemyRol = DB::table('usuario_rol')
            ->select('*')
            ->where('USR_USUARIO', $idUser)
            ->where('USR_ESTADO', 1)
            ->first();


            $listRolMenu = [];

            foreach ($response as $rol) {


                if($responsemyRol !== null ){

                    if($rol->ROL_CODIGO == $responsemyRol->USR_ROL){
                        array_push($listRolMenu, (object)['OP_CODIGO'=>$rol->ROL_CODIGO,'OP_NOMBRE'=>$rol->ROL_NOMBRE,'OP_ESTADO'=>1]);
    
                    }else{
    
                        array_push($listRolMenu, (object)['OP_CODIGO'=>$rol->ROL_CODIGO,'OP_NOMBRE'=>$rol->ROL_NOMBRE,'OP_ESTADO'=> 0]);
                    }

                }else{

                    array_push($listRolMenu, (object)['OP_CODIGO'=>$rol->ROL_CODIGO,'OP_NOMBRE'=>$rol->ROL_NOMBRE,'OP_ESTADO'=> 0]);

                }
                

            }


        return response()->json(["status" => true, "success" => true,"data" => $listRolMenu]);    
    
    } 
    
    public function activeRolUser(Request $request)
    {
       

        $responseAllRol = DB::table('usuario_rol')
            ->select('*')
            ->where([
                        ['USR_USUARIO', '=',  $request->usr_usuario]
                    ])
            -> get();


        foreach ($responseAllRol as $myrol) {

            $response = DB::table('usuario_rol')
                ->select('*')
                ->where('USR_CODIGO', $myrol->USR_CODIGO)
                ->update([
                            'USR_ESTADO'=> 0,
                        ]);


        }




        $response = DB::table('usuario_rol')
            ->select('*')
            ->where([
                        ['USR_USUARIO', '=',  $request->usr_usuario],
                        ['USR_ROL', '=', $request->usr_rol],
                    ])
            -> first();

            
       if($response !== NULL){


        

            $responsed = DB::table('usuario_rol')
                ->select('*')
                ->where('USR_CODIGO', $response->USR_CODIGO)
                ->update([
                            'USR_ESTADO'=> $request->usr_estado,
                        
                        ]);

            return response()->json(["status" => true, "success" => true, "message" => "Rol actualizado"]);

         

       }else{

            $id = DB::table('usuario_rol')->insertGetId([
                
                    "USR_ROL" => $request->usr_rol,
                    "USR_USUARIO" => $request->usr_usuario,
                    "USR_ESTADO" => $request->usr_estado,
                
                

            ]);

            return response()->json(["status" => true, "success" => true, "message" => "Rol actualizado"]);

       }


    }

    public function createPermisos(Request $request)
    {

        $id = DB::table('permisos')->insertGetId([
            
                "PER_NOMBRE" => $request->per_nombre,
                "PER_DESCRIPCION" => $request->per_descripcion,
                "PER_ESTADO" => 1,
            
            

        ]);

       
        return response()->json(["status" => true, "success" => true, "message" => "Rol creado con exitoso"]);
        

    }

    public function getallPermisos()
    {
        $response = DB::table('permisos')
            ->select('PER_CODIGO','PER_NOMBRE','PER_DESCRIPCION','PER_ESTADO')
            ->get();
            

        return response()->json($response);
    }


   

    public function getAllProductosVehiculo($idVehiculo)
    {
        
        $response = DB::table('veh_producto')
            ->join('producto', 'producto.pro_id', '=', 'veh_producto.producto')
            ->select('*')
            ->where('veh_producto.vehiculo', $idVehiculo)
            ->where('veh_producto.vep_estado', 1)
            ->where('veh_producto.vep_cantidad', '>', 0)
            ->get();
            

        return response()->json($response);
    }

    public function updatePermisos(Request $request)
    {


        $response = DB::table('permisos')
            ->select('*')
            ->where('PER_CODIGO', $request->per_id)
            ->update([
                        'PER_NOMBRE' => $request->per_nombre,
                        'PER_DESCRIPCION' => $request->per_descripcion,
                        'PER_ESTADO' => $request->per_estado,
                      

                    ]);

     
        return response()->json(["status" => true, "success" => true, "message" => "Permiso actualizado"]);

        
    }


    public function getListUserPermisos($idUser)
    {


        $listActivite = [];

        $response = DB::table('usuario_permisos')
            ->select('*')
            ->where('USPE_USUARIO',$idUser)
            ->get();


        $responseOpciones = DB::table('permisos')
            ->select('*')
            ->where('PER_ESTADO',1)
            ->get();    


        $listActiviteSelect = [];


       

            foreach ($responseOpciones as $opcion) {

                $flgaActive = false;


                foreach ($response as $user) {

                    if($opcion->PER_CODIGO == $user->USPE_PERMISO){

                        $flgaActive = true;

                        if($user->USPE_ESTADO == 0){

                            array_push($listActivite, (object)['OP_CODIGO'=>$opcion->PER_CODIGO,'OP_NOMBRE'=>$opcion->PER_NOMBRE,'OP_ESTADO'=> 0]);

                        } else if($user->USPE_ESTADO == 1){

                            array_push($listActivite, (object)['OP_CODIGO'=>$opcion->PER_CODIGO,'OP_NOMBRE'=>$opcion->PER_NOMBRE,'OP_ESTADO'=> 1]);
                        }
                        
                    }
                   

                }

                if(!$flgaActive){

                    array_push($listActivite, (object)['OP_CODIGO'=>$opcion->PER_CODIGO,'OP_NOMBRE'=>$opcion->PER_NOMBRE,'OP_ESTADO'=> 0]);
                    $flgaActive = false;

                }

            }

        return response()->json(["status" => true, "success" => true,"data" => $listActivite]);

    }

    public function activeUserPermisos(Request $request)
    {


        $responsePer = DB::table('usuario_permisos')
            ->select('*')
            ->where('USPE_USUARIO',$request->uspe_usuario)
            ->get();

            foreach ($responsePer as $per) {

                $response = DB::table('usuario_permisos')
                    ->select('*')
                    ->where('USPE_CODIGO', $per->USPE_CODIGO)
                    ->update([
                            "USPE_ESTADO" => 0,
                            ]);

            }


        $response = DB::table('usuario_permisos')
            ->select('*')
            ->where([
                        ['USPE_USUARIO', '=',  $request->uspe_usuario],
                        ['USPE_PERMISO', '=', $request->uspe_permiso],
                        ])
            -> first();

                
        if($response !== NULL){

                $responsed = DB::table('usuario_permisos')
                    ->select('*')
                    ->where('USPE_CODIGO', $response->USPE_CODIGO)
                    ->update([
                                'USPE_ESTADO'=> 1,
                            ]);



               

            
                return response()->json(["status" => true, "success" => true, "message" => "Permiso actualizado"]);

            

        }else{

                $id = DB::table('usuario_permisos')->insertGetId([
                    
                        "USPE_USUARIO" => $request->uspe_usuario,
                        "USPE_PERMISO" => $request->uspe_permiso,
                        "USPE_ESTADO" => 1,
                      
                    

                ]);


               


                return response()->json(["status" => true, "success" => true, "message" => "Permiso actualizado"]);

        }

    }


    public function getListUserEmpresas($idUser)
    {


        $listActivite = [];
   
        $responsePermisos = DB::table('usuario_permisos')
            ->select('*')
            ->where('USPE_USUARIO',$idUser)
            ->where('USPE_PERMISO',2)
            ->where('USPE_ESTADO',1)
            ->first();

            

        if($responsePermisos !== NULL){


            $response = DB::table('usuario_empresa1')
                ->select('*')
                ->where('USE_USUARIO',$idUser)
                ->get();


            $responseOpciones = DB::table('empresa1')
                ->select('*')
                ->where('EMP_ESTADO',1)
                ->get();    


            $listActiviteSelect = [];


       

            foreach ($responseOpciones as $opcion) {

                $flgaActive = false;


                foreach ($response as $user) {

                    if($opcion->EMP_CODIGO == $user->USE_EMPRESA){

                        $flgaActive = true;

                        if($user->USE_ESTADO == 0){

                            array_push($listActivite, (object)['OP_CODIGO'=>$opcion->EMP_CODIGO,'OP_NOMBRE'=>$opcion->EMP_NOMBRE_COMERCIAL,'OP_ESTADO'=> 0]);

                        } else if($user->USE_ESTADO == 1){

                            array_push($listActivite, (object)['OP_CODIGO'=>$opcion->EMP_CODIGO,'OP_NOMBRE'=>$opcion->EMP_NOMBRE_COMERCIAL,'OP_ESTADO'=> 1]);
                        }
                        
                    }
                   

                }

                if(!$flgaActive){

                    array_push($listActivite, (object)['OP_CODIGO'=>$opcion->EMP_CODIGO,'OP_NOMBRE'=>$opcion->EMP_NOMBRE_COMERCIAL,'OP_ESTADO'=> 0]);
                    $flgaActive = false;

                }

            }

            return response()->json(["status" => true, "success" => true,"data" => $listActivite]);


        }else{
            return response()->json(["status" => true, "success" => false,"data" => 'No tiene Permisos de Empresa','message' => 'No tiene Permisos de Empresa']);
        }


        

    }

    public function register(){
        $user =new User(request()->all());
        $user->password = bcrypt($user->password);
        $user->idUsuario = 1;
        $user->save();
        return response()->json(["data"=>$user],200);
    }

    public function login2(Request $request)
    {

       

        
          $credentials = request(['email', 'password']);

        
        //  $credentials = new Request([
        //     'password'   => '0301945408',
        //     'email'  => 'caaf1025@gmail.com',
        // ]);

        $hola = response()->json(["password" => "0301945408", "email" => "1011jc@hotmail.es"]);

        $responseperson = DB::table('usuario')
            ->select('*')
            ->where('US_USUARIO',$request->email)
            ->where('US_ESTADO',1)
            ->first();

            // Log::debug('An informational message.');
            // error_log($credentials);

            // $credentials = request([$responseperson->US_USUARIO, $responseperson->US_CEDULA]);
      
        //    $jwt_token = JWTAuth::attempt($credentials);

        // //    (!$token = \Auth::guard('api')->attempt($credentials)) 

      

        $myTTL = 2; //minutes

        // JWTAuth::fac JWTAuth::factory()->setTTL($myTTL);tory()->setTTL($expirationInMinutes);
        JWTAuth::factory()->setTTL($myTTL);
// JWTAuth::attempt($credentials);

        // JWTAuth::factory()->setTTL($myTTL);
        // $token = $this->guard()->attempt($hola->original);
        $token = JWTAuth::attempt($hola->original);
        return response()->json(["status" => true, "success" => false,"token" => $token,"cre"=>$credentials,"hola"=> $hola->original]);

        
        // $responseperson = DB::table('usuario')
        //     ->select('*')
        //     ->where('US_USUARIO',$usuario)
        //     ->where('US_ESTADO',1)
        //     ->first();


        // if($responseperson !== NULL){

        //     if($responseperson->US_CONTRASENIA == $pass){


        //         $rolperson = DB::table('usuario_rol')
        //             ->join('roles', 'roles.ROL_CODIGO', '=', 'usuario_rol.USR_ROL')
        //             ->select('*')
        //             ->where('USR_USUARIO',$responseperson->US_CODIGO)
        //             ->where('USR_ESTADO',1)
        //             ->first();


        //         $menus = DB::table('rol_menu')
        //             ->join('roles', 'roles.ROL_CODIGO', '=', 'rol_menu.ROM_ROL')
        //             ->join('menu', 'menu.MEN_CODIGO', '=', 'rol_menu.ROM_MENU')
        //             ->join('modulos','modulos.MO_CODIGO','=','menu.MEN_MODULO')
        //             ->select('menu.MEN_CODIGO','menu.MEN_NOMBRE','menu.MEN_DESCRIPCION','menu.MEN_ICONO','modulos.MO_CODIGO','modulos.MO_NOMBRE','modulos.MO_DESCRIPCION')
        //             ->where('ROM_ROL',$rolperson->ROL_CODIGO)
        //             ->where('ROM_ESTADO',1)
        //             ->orderBy('modulos.MO_CODIGO')
        //             ->get();

       


        //         $listaModulos = [];
        //         $listaMenus = [];
        //         $codigoModulo = 0 ;
        //         $nombreModulo = '' ;
        //         $descripcionModulo = '' ;

        //         foreach ($menus as $menu) {
        //             $opciones = DB::table('rol_menu_opcion')
        //                 ->join('roles', 'roles.ROL_CODIGO', '=', 'rol_menu_opcion.ROLMO_ROL')
        //                 ->join('opciones', 'opciones.op_codigo', '=', 'rol_menu_opcion.ROLMO_OPCIONES')
        //                 ->join('menu', 'menu.MEN_CODIGO', '=', 'rol_menu_opcion.ROLMO_MENU')
        //                 ->select('opciones.op_codigo','opciones.op_nombre as label','opciones.op_url as to')
        //                 ->where('ROLMO_ROL',$rolperson->ROL_CODIGO)
        //                 ->where('ROLMO_MENU',$menu->MEN_CODIGO)
        //                 ->where('ROLMO_ESTADO',1)
        //                 ->get();

        //             if($codigoModulo == 0){

        //                 $codigoModulo = $menu->MO_CODIGO;
        //                 $nombreModulo = $menu->MO_NOMBRE;
        //                 $descripcionModulo = $menu->MO_DESCRIPCION;

        //                 array_push($listaMenus, (object)['MEN_CODIGO'=>$menu->MEN_CODIGO,'MEN_NOMBRE'=>$menu->MEN_NOMBRE,'MEN_DESCRIPCION'=>$menu->MEN_DESCRIPCION,'MEN_ICONO'=>$menu->MEN_ICONO,'MEN_OPCIONES'=>$opciones]);

        //             }else{

        //                 if($codigoModulo == $menu->MO_CODIGO){

        //                     array_push($listaMenus, (object)['MEN_CODIGO'=>$menu->MEN_CODIGO,'MEN_NOMBRE'=>$menu->MEN_NOMBRE,'MEN_DESCRIPCION'=>$menu->MEN_DESCRIPCION,'MEN_ICONO'=>$menu->MEN_ICONO,'MEN_OPCIONES'=>$opciones]);
                        
        //                 }else{

        //                     array_push($listaModulos, (object)['MO_CODIGO'=>$codigoModulo,'MO_NOMBRE'=>$nombreModulo,'MO_DESCRIPCION'=>$descripcionModulo,'MO_MENU'=>$listaMenus]);
        //                     $codigoModulo = $menu->MO_CODIGO;
        //                     $nombreModulo = $menu->MO_NOMBRE;
        //                     $descripcionModulo = $menu->MO_DESCRIPCION;
        //                     $listaMenus = [];
        //                     array_push($listaMenus, (object)['MEN_CODIGO'=>$menu->MEN_CODIGO,'MEN_NOMBRE'=>$menu->MEN_NOMBRE,'MEN_DESCRIPCION'=>$menu->MEN_DESCRIPCION,'MEN_ICONO'=>$menu->MEN_ICONO,'MEN_OPCIONES'=>$opciones]);


        //                 }
        //             }
        //         }
        //         array_push($listaModulos, (object)['MO_CODIGO'=>$codigoModulo,'MO_NOMBRE'=>$nombreModulo,'MO_DESCRIPCION'=>$descripcionModulo,'MO_MENU'=>$listaMenus]);



        //         $permisos = DB::table('usuario_permisos')
        //             ->join('permisos', 'permisos.PER_CODIGO', '=', 'usuario_permisos.USPE_PERMISO')
        //             ->select('permisos.PER_CODIGO','permisos.PER_NOMBRE','permisos.PER_DESCRIPCION')
        //             ->where('USPE_USUARIO',$responseperson->US_CODIGO)
        //             ->where('USPE_ESTADO',1)
        //             ->get();
            
                
        //         $listaEmpresas = [];

        //         if(DB::table('usuario_permisos')
        //             ->where('USPE_USUARIO',$responseperson->US_CODIGO)
        //             ->where('USPE_ESTADO',1)
        //             ->where('USPE_PERMISO',2)
        //             ->exists())
        //         {

        //             $empresa = DB::table('usuario_empresa1')
        //                 ->join('empresa1', 'empresa1.EMP_CODIGO', '=', 'usuario_empresa.USE_EMPRESA')
        //                 ->select('*')
        //                 ->where('USE_USUARIO',$responseperson->US_CODIGO)
        //                 ->where('USE_ESTADO',1)
        //                 ->get();

        //             $listaEmpresas = $empresa;

        //         }


        //        $objectResp = (object)['US_CODIGO'=>$responseperson->US_CODIGO,'US_CEDULA'=>$responseperson->US_CEDULA,'US_NOMBRE'=>$responseperson->US_NOMBRE,'US_APELLIDO'=>$responseperson->US_APELLIDO,'US_DIRECCION'=>$responseperson->US_DIRECCION,'US_TELEFONO'=>$responseperson->US_TELEFONO,'US_CELULAR'=>$responseperson->US_CELULAR,'US_ROL'=>$rolperson->ROL_NOMBRE,'US_EMAIL'=>$responseperson->US_EMAIL,'US_CARGO'=>$rolperson->ROL_NOMBRE,'US_MODULO'=>$listaModulos,'US_PERMISOS'=>$permisos,'US_EMPRESAS'=>$listaEmpresas,'US_EMPRESA_DEFAULT'=>null];
                
               
        //        return response()->json(["status" => true, "success" => true,"autenticado" => true, "data" => $objectResp,'message' => 'Logeo Correcto']);


        //     }else{

        //         return response()->json(["status" => true, "success" => false,"data" => 'Contraseña Incorrecta','message' => 'Contraseña Incorrecta']);

        //     }


        // }else{

        //     return response()->json(["status" => true, "success" => false,"data" => 'Usuario no existe comuniquese con el administrador','message' => 'Usuario no existe comuniquese con el administrador']);

        // }
        


    }

    public function login($usuario,$pass)
    {

        
        // $credentials = request(['email', 'password']);
        // $hola = response()->json(["password" => "0301945408", "email" => "1011jc@hotmail.es"]);

        // $responseperson = DB::table('usuario')
        //     ->select('*')
        //     ->where('US_USUARIO',$request->email)
        //     ->where('US_ESTADO',1)
        //     ->first();

        // $myTTL = 60; //minutes
        // JWTAuth::factory()->setTTL($myTTL);

        // $token = JWTAuth::attempt($hola->original);
        // return response()->json(["status" => true, "success" => false,"token" => $token,"cre"=>$credentials,"hola"=> $hola->original]);

        ///////************************************************************ */
       
       
        $responseperson = DB::table('usuario')
            ->select('*')
            ->where('US_USUARIO',$usuario)
            ->where('US_ESTADO',1)
            ->first();


        if($responseperson !== NULL){

            if($responseperson->US_CONTRASENIA == $pass){


                $rolperson = DB::table('usuario_rol')
                    ->join('roles', 'roles.ROL_CODIGO', '=', 'usuario_rol.USR_ROL')
                    ->select('*')
                    ->where('USR_USUARIO',$responseperson->US_CODIGO)
                    ->where('USR_ESTADO',1)
                    ->first();


                $menus = DB::table('rol_menu')
                    ->join('roles', 'roles.ROL_CODIGO', '=', 'rol_menu.ROM_ROL')
                    ->join('menu', 'menu.MEN_CODIGO', '=', 'rol_menu.ROM_MENU')
                    ->join('modulos','modulos.MO_CODIGO','=','menu.MEN_MODULO')
                    ->select('menu.MEN_CODIGO','menu.MEN_NOMBRE','menu.MEN_DESCRIPCION','menu.MEN_ICONO','modulos.MO_CODIGO','modulos.MO_NOMBRE','modulos.MO_DESCRIPCION')
                    ->where('ROM_ROL',$rolperson->ROL_CODIGO)
                    ->where('ROM_ESTADO',1)
                    ->orderBy('modulos.MO_CODIGO')
                    ->get();

       


                $listaModulos = [];
                $listaMenus = [];
                $codigoModulo = 0 ;
                $nombreModulo = '' ;
                $descripcionModulo = '' ;

                foreach ($menus as $menu) {
                    $opciones = DB::table('rol_menu_opcion')
                        ->join('roles', 'roles.ROL_CODIGO', '=', 'rol_menu_opcion.ROLMO_ROL')
                        ->join('opciones', 'opciones.op_codigo', '=', 'rol_menu_opcion.ROLMO_OPCIONES')
                        ->join('menu', 'menu.MEN_CODIGO', '=', 'rol_menu_opcion.ROLMO_MENU')
                        ->select('opciones.op_codigo','opciones.op_nombre as label','opciones.op_url as to')
                        ->where('ROLMO_ROL',$rolperson->ROL_CODIGO)
                        ->where('ROLMO_MENU',$menu->MEN_CODIGO)
                        ->where('ROLMO_ESTADO',1)
                        ->get();

                    if($codigoModulo == 0){

                        $codigoModulo = $menu->MO_CODIGO;
                        $nombreModulo = $menu->MO_NOMBRE;
                        $descripcionModulo = $menu->MO_DESCRIPCION;

                        array_push($listaMenus, (object)['MEN_CODIGO'=>$menu->MEN_CODIGO,'MEN_NOMBRE'=>$menu->MEN_NOMBRE,'MEN_DESCRIPCION'=>$menu->MEN_DESCRIPCION,'MEN_ICONO'=>$menu->MEN_ICONO,'MEN_OPCIONES'=>$opciones]);

                    }else{

                        if($codigoModulo == $menu->MO_CODIGO){

                            array_push($listaMenus, (object)['MEN_CODIGO'=>$menu->MEN_CODIGO,'MEN_NOMBRE'=>$menu->MEN_NOMBRE,'MEN_DESCRIPCION'=>$menu->MEN_DESCRIPCION,'MEN_ICONO'=>$menu->MEN_ICONO,'MEN_OPCIONES'=>$opciones]);
                        
                        }else{

                            array_push($listaModulos, (object)['MO_CODIGO'=>$codigoModulo,'MO_NOMBRE'=>$nombreModulo,'MO_DESCRIPCION'=>$descripcionModulo,'MO_MENU'=>$listaMenus]);
                            $codigoModulo = $menu->MO_CODIGO;
                            $nombreModulo = $menu->MO_NOMBRE;
                            $descripcionModulo = $menu->MO_DESCRIPCION;
                            $listaMenus = [];
                            array_push($listaMenus, (object)['MEN_CODIGO'=>$menu->MEN_CODIGO,'MEN_NOMBRE'=>$menu->MEN_NOMBRE,'MEN_DESCRIPCION'=>$menu->MEN_DESCRIPCION,'MEN_ICONO'=>$menu->MEN_ICONO,'MEN_OPCIONES'=>$opciones]);


                        }
                    }
                }
                array_push($listaModulos, (object)['MO_CODIGO'=>$codigoModulo,'MO_NOMBRE'=>$nombreModulo,'MO_DESCRIPCION'=>$descripcionModulo,'MO_MENU'=>$listaMenus]);



                $permisos = DB::table('usuario_permisos')
                    ->join('permisos', 'permisos.PER_CODIGO', '=', 'usuario_permisos.USPE_PERMISO')
                    ->select('permisos.PER_CODIGO','permisos.PER_NOMBRE','permisos.PER_DESCRIPCION')
                    ->where('USPE_USUARIO',$responseperson->US_CODIGO)
                    ->where('USPE_ESTADO',1)
                    ->get();
            
                
                $listaEmpresas = [];

                if(DB::table('usuario_permisos')
                    ->where('USPE_USUARIO',$responseperson->US_CODIGO)
                    ->where('USPE_ESTADO',1)
                    ->where('USPE_PERMISO',2)
                    ->exists())
                {

                    $empresa = DB::table('usuario_empresa1')
                        ->join('empresa1', 'empresa1.EMP_CODIGO', '=', 'usuario_empresa1.USE_EMPRESA')
                        ->select('*')
                        ->where('USE_USUARIO',$responseperson->US_CODIGO)
                        ->where('USE_ESTADO',1)
                        ->first();

                    $listaEmpresas = $empresa;

                }


            
                $tokenPerson = response()->json(["password" => $responseperson->US_TOKEN_PASS, "email" => $responseperson->US_EMAIL_TOKEN]);
                $myTTL = 360; //minutes
                JWTAuth::factory()->setTTL($myTTL);

                $token = JWTAuth::attempt($tokenPerson->original);


               $objectResp = (object)['US_CODIGO'=>$responseperson->US_CODIGO,'US_CEDULA'=>$responseperson->US_CEDULA,'US_NOMBRE'=>$responseperson->US_NOMBRE,'US_APELLIDO'=>$responseperson->US_APELLIDO,'US_DIRECCION'=>$responseperson->US_DIRECCION,'US_TELEFONO'=>$responseperson->US_TELEFONO,'US_CELULAR'=>$responseperson->US_CELULAR,'US_ROL'=>$rolperson->ROL_NOMBRE,'US_EMAIL'=>$responseperson->US_EMAIL,'US_CARGO'=>$rolperson->ROL_NOMBRE,'US_MODULO'=>$listaModulos,'US_PERMISOS'=>$permisos,'US_EMPRESAS'=>$listaEmpresas,'US_EMPRESA_DEFAULT'=>null,"TOKEN"=>$token];
                
               
               return response()->json(["status" => true, "success" => true,"autenticado" => true, "data" => $objectResp,'message' => 'Logeo Correcto']);


            }else{

                return response()->json(["status" => true, "success" => false,"data" => 'Contraseña Incorrecta','message' => 'Contraseña Incorrecta']);

            }


        }else{

            return response()->json(["status" => true, "success" => false,"data" => 'Usuario no existe comuniquese con el administrador','message' => 'Usuario no existe comuniquese con el administrador']);

        }
        


    }

    public function activeUserEmpresa(Request $request)
    {



        $response = DB::table('usuario_empresa1')
            ->select('*')
            ->where([
                        ['USE_USUARIO', '=',  $request->use_usuario],
                        ['USE_EMPRESA', '=', $request->use_empresa],
                        ])
            -> first();

                
        if($response !== NULL){

                $responsed = DB::table('usuario_empresa1')
                    ->select('*')
                    ->where('USE_CODIGO', $response->USE_CODIGO)
                    ->update([
                                'USE_ESTADO'=> $request->use_estado,
                              
                            ]);


                    
                return response()->json(["status" => true, "success" => true, "message" => "Empresa Actualizada con Exito"]);

            

        }else{

                $id = DB::table('usuario_empresa1')->insertGetId([
                        "USE_USUARIO" => $request->use_usuario,
                        "USE_EMPRESA" => $request->use_empresa,
                        "USE_ESTADO" => $request->use_estado,
                      
                ]);


          


                return response()->json(["status" => true, "success" => true, "message" => "Empresa Actualizada con Exito"]);

        }

    }

    public function deleteClienteRuta(Request $request)
    {

        DB::table('ruta_cliente')
                ->select('*')
                ->where('cliente', $request->cliente)
                ->where('ruta', $request->ruta)
                ->update([
                    "ru_cli_estado" => 0, 
                        ]);

        
    }


    public function deleteProductoPromociosn(Request $request)
    {
        DB::table('promocion_producto')
                ->select('*')
                ->where('prp_id', $request->prp_id)
                ->update([
                    "prp_estado" => 0, 
                        ]);   
    }


    public function designarEmplieado(Request $request)
    {

        if($request->tipo == 1){
            DB::table('ruta')
                ->select('*')
                ->where('ru_id', $request->ruta)
                ->update([
                    "empleado" => $request->empleado, 
                        ]);

        }

        else if($request->tipo == 2){

            DB::table('ruta')
                ->select('*')
                ->where('ru_id', $request->ruta)
                ->update([
                    "chofer" => $request->empleado, 
                        ]);

        }
        

        
    }



    public function designarEmpleadoAuto(Request $request)
    {
            DB::table('vehiculo')
                ->select('*')
                ->where('veh_id', $request->veh_id)
                ->update([
                    "empleado" => $request->empleado, 
                        ]);
    }

    public function designarImpresora(Request $request)
    {
            DB::table('vehiculo')
                ->select('*')
                ->where('veh_id', $request->veh_id)
                ->update([
                    "impresora" => $request->impresora, 
                        ]);
    }

    public function crearCompra(Request $request)
    {


        if($request->tipo_compra == 1){
            $factura = DB::table('compra')->insertGetId([
                "com_cliente" => $request->com_cliente, 
                "com_fecha" => $request->com_fecha, 
                "com_descuento" => $request->com_descuento, 
                "com_sub_total" => $request->com_sub_total, 
                "com_total" => $request->com_total, 
                "com_iva" => $request->com_iva, 
                "empleado_id" => $request->empleado_id, 
                "com_tipo" => $request->com_tipo, 
                "emp_id" => $request->emp_id, 
            ]);
    
            $listaor =$request->input('lista_detalle');
            $lista = json_decode($listaor, true);
    
            foreach ($lista as $orden) {
    
                $empresa = DB::table('detalle_compra')->insert([
                    "dcom_producto" => $orden['pro_id'],
                    "dcom_compra" => $factura,
                    "dcom_cantidad" => (int)$orden['pro_cantidad'],
                    "dcom_precio_unitario" => (float)$orden['unitario'],
                    "dcom_precio_total" => (float)$orden['total'],
                ]);


                if($orden['pro_tipo'] == 1){

                    $kardex = DB::table('kardex')->insert([
                        "ka_producto" => $orden['pro_id'], 
                        "ka_tipo" => 2, 
                        "ka_cantidad" => (int)$orden['pro_cantidad'], 
                        "ka_fecha_ingreso" => $request->com_fecha, 
                        "ka_descripcion" => "Compra mediante Web"
                    ]);
            
                    $response = DB::table('producto')
                        ->select('*')
                        ->where('pro_id', $orden['pro_id'])
                        ->first();
                        
                    $cantidad = $response->pro_stop;
            
                    $cantidad = $cantidad - (int)$orden['pro_cantidad'];
        
                    if($cantidad < 0){
                        $cantidad = 0;
                    }
                    
            
                    $responseProducto = DB::table('producto')
                    ->select('*')
                    ->where('pro_id', $orden['pro_id'])
                    ->update([
                        "pro_stop" => $cantidad, 
                            ]);

                }

                else if($orden['pro_tipo'] == 2){


                    $productosPromocion = DB::table('promocion_producto')
                        ->select('*')
                        ->where('promocion',$orden['pro_id'])
                        ->where('prp_estado',1)
                        ->get();

                    foreach ($productosPromocion as $producto) {

                        $kardex = DB::table('kardex')->insert([
                            "ka_producto" => $producto->producto, 
                            "ka_tipo" => 2, 
                            "ka_cantidad" => ((int)$producto->prp_cantidad * (int)$orden['pro_cantidad']), 
                            "ka_fecha_ingreso" => $request->com_fecha, 
                            "ka_descripcion" => "Compra mediante Web"
                        ]);
                
                        $response = DB::table('producto')
                            ->select('*')
                            ->where('pro_id', $producto->producto)
                            ->first();
                            
                        $cantidad = $response->pro_stop;
                
                        $cantidad = $cantidad - ((int)$producto->prp_cantidad * (int)$orden['pro_cantidad']);
            
                        if($cantidad < 0){
                            $cantidad = 0;
                        }
                        
                
                        $responseProducto = DB::table('producto')
                        ->select('*')
                        ->where('pro_id', $producto->producto)
                        ->update([
                            "pro_stop" => $cantidad, 
                                ]);


                        }

                }
    
                
               
            }

        }

    }

    public function crearVentaAuto(Request $request)
    {
        $vehiculo = DB::table('vehiculo')
            ->select('*')
            ->where('veh_id',$request->idVehiculo)
            ->first();

        $autoventa =  DB::table('autoventa')->insertGetId([
                    "aven_fecha" => $request->aven_fecha, 
                    "empleado" =>$vehiculo->empleado,
                    "aven_descripcion" =>"Ingreso de web",
                ]);


        $listaor =$request->input('listaordenes');
        $listaordenes = json_decode($listaor, true);

        foreach ($listaordenes as $ordenes) {

            $factura = DB::table('compra')->insertGetId([
                        "com_cliente" => $ordenes['com_cliente'], 
                        "com_fecha" => $request->aven_fecha, 
                        "com_descuento" => $ordenes['com_descuento'], 
                        "com_sub_total" => $ordenes['com_sub_total'], 
                        "com_total" => $ordenes['com_total'], 
                        "com_iva" => $ordenes['com_iva'], 
                        "empleado_id" => $vehiculo->empleado , 
                        "com_tipo" => $ordenes['com_tipo'], 
                        "emp_id" => $ordenes['emp_id'], 
                        "autoventa" => $autoventa 
                    ]);
            
                // $listaor =$request->input('lista_detalle');
                    $lista = $ordenes['lista_detalle'];
            
                    foreach ($lista as $orden) {
            
                        $empresa = DB::table('detalle_compra')->insert([
                            "dcom_producto" => $orden['pro_id'],
                            "dcom_compra" => $factura,
                            "dcom_cantidad" => (int)$orden['pro_cantidad'],
                            "dcom_precio_unitario" => (float)$orden['unitario'],
                            "dcom_precio_total" => (float)$orden['total'],
                        ]);

                        $responsea = DB::table('veh_producto')
                            ->select('*')
                            ->where('vehiculo', $request->idVehiculo)
                            ->where('producto', $orden['pro_id'])
                            ->first();

                        if($responsea == NULL){}
                        else{

                            $newCantidad = $responsea->vep_cantidad - (int)$orden['pro_cantidad'];

                            DB::table('veh_producto')
                                ->select('*')
                                ->where('vehiculo', $request->idVehiculo)
                                ->where('producto', $orden['pro_id'])
                                ->update([
                                    "vep_cantidad" => $newCantidad, 
                                        ]);


                        }


        }

    

        
        }

    }

    public function createEmpleado(Request $request)
    {
        $empresa = DB::table('empleado')->insert([
            "emp_ci" => $request->emp_ci, 
            "emp_nombre" => $request->emp_nombre, 
            "emp_apellido" => $request->emp_apellido, 
            "emp_telefono" => $request->emp_telefono, 
            "emp_usuario" => $request->emp_usuario, 
            "emp_contrasena" => $request->emp_contrasena, 
            "emp_tipo" => $request->emp_tipo, 
            "emp_estado" => 1, 
            "emp_usuario_empresa_id" => $request->emp_usuario_empresa_id, 
        ]);

    }

    public function createcliente(Request $request)
    {

        $empresa = DB::table('cliente')->insert([
            "cli_nombre" => $request->cli_nombre, 
            "cli_ci" => $request->cli_ci, 
            "cli_apellido" => $request->cli_apellido, 
            "cli_telefono" => $request->cli_telefono, 
            "cli_direccion" => $request->cli_direccion, 
            "cli_latitud" => $request->cli_latitud, 
            "cli_email" => $request->cli_email, 
            "cli_longitud" => $request->cli_longitud, 
            "cli_empresa_id" => $request->cli_empresa_id, 
        ]);

    }

    public function updateCliente(Request $request)
    {
        $response = DB::table('cliente')
            ->select('*')
            ->where('cli_id', $request->cli_id)
            ->update([
                "cli_nombre" => $request->cli_nombre, 
                "cli_apellido" => $request->cli_apellido, 
                "cli_telefono" => $request->cli_telefono, 
                "cli_direccion" => $request->cli_direccion, 
                "cli_latitud" => $request->cli_latitud, 
                "cli_email" => $request->cli_email, 
                "cli_longitud" => $request->cli_longitud, 
                    ]);
    }

    public function updateimpresora(Request $request)
    {

        $response = DB::table('impresora')
            ->select('*')
            ->where('imp_id', $request->imp_id)
            ->update([
                "imp_nombre" => $request->imp_nombre, 
                "imp_mac" => $request->imp_mac, 
                    ]);
   
    }


    public function updateRuta(Request $request)
    {

        $response = DB::table('ruta')
            ->select('*')
            ->where('ru_id', $request->ru_id)
            ->update([
                "ru_descripcion" => $request->ru_descripcion, 
                "ru_dia" => $request->ru_dia
             ]);
   
    }

    public function updatevehiculo(Request $request)
    {

        $response = DB::table('vehiculo')
            ->select('*')
            ->where('veh_id', $request->veh_id)
            ->update([
                "veh_placa" => $request->veh_placa, 
                "veh_anio" => $request->veh_anio,
                "veh_color" => $request->veh_color,
                "veh_marca" => $request->veh_marca,
             ]);
   
    }

    public function getProveedores($idEmpresa){

        $allEmpresas = DB::table('empresa')
            ->select('*')
            ->where('emp_estado',1)
            ->where('emp_usuario_empresa_id',$idEmpresa)
            ->get();

        return response()->json($allEmpresas);

    }

    public function getOrdenesPreventa($idEmpresa){

        $allPreventa = DB::table('compra')
            ->join('empleado','compra.empleado_id', '=', 'empleado.emp_id')
            ->join('cliente','compra.com_cliente', '=', 'cliente.cli_id')
            ->select('*')
            ->where('compra.emp_id',$idEmpresa)
            ->where('compra.com_tipo',2)
            ->get();

        return response()->json($allPreventa);

    }


    public function getOrdenesFecha($idEmpresa,$fechaInicio, $fechaFin){

        $allPreventa = DB::table('compra')
            ->join('empleado','compra.empleado_id', '=', 'empleado.emp_id')
            ->join('cliente','compra.com_cliente', '=', 'cliente.cli_id')
            ->select('*')
            ->where('compra.emp_id',$idEmpresa)
            ->whereBetween('com_fecha', [$fechaInicio, $fechaFin])
            ->get();

        return response()->json($allPreventa);

    }

    public function getOrdenesFechaCliente($idEmpresa,$fechaInicio, $fechaFin,$numeroCliente){

        $allPreventa = DB::table('compra')
            ->join('empleado','compra.empleado_id', '=', 'empleado.emp_id')
            ->join('cliente','compra.com_cliente', '=', 'cliente.cli_id')
            ->select('*')
            ->where('compra.emp_id',$idEmpresa)
            ->where('cliente.cli_ci',$numeroCliente)
            ->whereBetween('com_fecha', [$fechaInicio, $fechaFin])
            ->get();

        return response()->json($allPreventa);

    }

    public function getOrdenesFechaVendedor($idEmpresa,$fechaInicio, $fechaFin,$numeroCliente){

        $allPreventa = DB::table('compra')
            ->join('empleado','compra.empleado_id', '=', 'empleado.emp_id')
            ->join('cliente','compra.com_cliente', '=', 'cliente.cli_id')
            ->select('*')
            ->where('compra.emp_id',$idEmpresa)
            ->where('empleado.emp_ci',$numeroCliente)
            ->whereBetween('com_fecha', [$fechaInicio, $fechaFin])
            ->get();

        return response()->json($allPreventa);

    }

    public function getItmesOrdenes($idOrden){

        $allPreventa = DB::table('detalle_compra')
            ->join('producto','detalle_compra.dcom_producto', '=', 'producto.pro_id')
            ->select('*')
            ->where('dcom_compra',$idOrden)
            ->get();

        return response()->json($allPreventa);

    }

    public function getRutas($idEmpresa){

        $allRutas = DB::table('ruta')
            ->select('*')
            ->where('ru_estado',1)
            ->where('empresa',$idEmpresa)
            ->get();

       

        $listaEmpleadoRuta = [];

        foreach ($allRutas as $ruta) {

            if($ruta->empleado == 0){

                array_push($listaEmpleadoRuta, (object)['ru_id'=>$ruta->ru_id,'ru_descripcion'=>$ruta->ru_descripcion,'ru_dia'=> $ruta->ru_dia,'empleado'=>"No Designado"]);


            }else{

                $empleado = DB::table('empleado')
                    ->select('*')
                    ->where('emp_id', $ruta->empleado)
                    ->first();

                array_push($listaEmpleadoRuta, (object)['ru_id'=>$ruta->ru_id,'ru_descripcion'=>$ruta->ru_descripcion,'ru_dia'=> $ruta->ru_dia,'empleado'=>$empleado->emp_nombre." ".$empleado->emp_apellido]);


            }

        }

        return response()->json($listaEmpleadoRuta);

    }


    public function getvehiculos($idEmpresa){

        $allRutas = DB::table('vehiculo')
            ->select('*')
            ->where('veh_estado',1)
            ->where('empresa',$idEmpresa)
            ->get();

       

        $listaEmpleadoRuta = [];

        foreach ($allRutas as $ruta) {

            if($ruta->empleado == 0){

                array_push($listaEmpleadoRuta, (object)['veh_id'=>$ruta->veh_id,'veh_placa'=>$ruta->veh_placa,'veh_anio'=> $ruta->veh_anio,'veh_color'=> $ruta->veh_color,'veh_marca'=> $ruta->veh_marca,'empleado'=>"No Designado",'id_empleado'=>0]);


            }else{

                $empleado = DB::table('empleado')
                    ->select('*')
                    ->where('emp_id', $ruta->empleado)
                    ->first();

                array_push($listaEmpleadoRuta, (object)['veh_id'=>$ruta->veh_id,'veh_placa'=>$ruta->veh_placa,'veh_anio'=> $ruta->veh_anio,'veh_color'=> $ruta->veh_color,'veh_marca'=> $ruta->veh_marca,'empleado'=>$empleado->emp_nombre." ".$empleado->emp_apellido,'id_empleado'=>$empleado->emp_id]);


            }

        }

        return response()->json($listaEmpleadoRuta);

    }

    public function getClienteForciNombre($textoApp,$idEmpresa){
        $results = DB::select( DB::raw("SELECT * FROM appDistribucionesdb.cliente c WHERE (upper(c.cli_nombre) like '%$textoApp%' OR upper(c.cli_apellido) like '%$textoApp%' OR upper(c.cli_ci) like '%$textoApp%' ) and c.cli_empresa_id = $idEmpresa") );
        return response()->json($results);
    }


    public function getClientesRuta($idRuta){


        $misClientes = DB::table('ruta_cliente')
            ->join('cliente','ruta_cliente.cliente', '=', 'cliente.cli_id')
            ->select('*')
            ->where('ru_cli_estado',1)
            ->where('ruta',$idRuta)
            ->get();

        return response()->json($misClientes);

    }
    


    public function getProductosTexto($textoApp,$idEmpresa){

        $results = DB::select( DB::raw("SELECT * FROM appDistribucionesdb.producto pro WHERE upper(pro.pro_nombre) like '%$textoApp%' and pro.pro_empresa_distribuidora = $idEmpresa") );
        return response()->json($results);

    }

    public function getClientesNoDesignados($idEmpresa){

        $allClientes = DB::table('cliente')
            ->select('*')
            ->where('cli_empresa_id',$idEmpresa)
            ->get();

        $clientesSeleccionados = [];
        foreach ($allClientes as $cliente) {

            $clienteSelec = DB::table('ruta_cliente')
                    ->select('*')
                    ->where('cliente', $cliente->cli_id)
                    ->where('ru_cli_estado', 1)
                    ->first();

            if($clienteSelec === NULL){

                array_push($clientesSeleccionados, (object)['cli_id'=>$cliente->cli_id,'cli_ci'=>$cliente->cli_ci,'cli_nombre'=> $cliente->cli_nombre,'cli_apellido'=> $cliente->cli_apellido,'cli_direccion'=> $cliente->cli_direccion]);

            }

        }


        return response()->json($clientesSeleccionados);
    }

    public function getEmpleados($idEmpresa){

        $allEmpleados = DB::table('empleado')
            ->select('*')
            ->where('emp_usuario_empresa_id',$idEmpresa)
            ->get();
        return response()->json($allEmpleados);
    }

    public function getEmpleadosVendedor($idEmpresa){

        $allEmpleados = DB::table('empleado')
            ->select('*')
            ->where('emp_usuario_empresa_id',$idEmpresa)
            ->where('emp_tipo',1)
            ->get();
        return response()->json($allEmpleados);
    }

    public function getEmpleadosChofer($idEmpresa){

        $allEmpleados = DB::table('empleado')
            ->select('*')
            ->where('emp_usuario_empresa_id',$idEmpresa)
            ->where('emp_tipo',2)
            ->get();
        return response()->json($allEmpleados);
    }

    public function getimpresoras($idEmpresa){

        $allEmpleados = DB::table('impresora')
            ->select('*')
            ->where('empresa',$idEmpresa)
            ->get();

        return response()->json($allEmpleados);
    }

    public function getclientes($idEmpresa){

        $allClientes = DB::table('cliente')
            ->select('*')
            ->where('cli_empresa_id',$idEmpresa)
            ->get();
        return response()->json($allClientes);
    }


    public function getProductosEmpresa($idEmpresa){

        $allProductos = DB::table('producto')
            ->select('*')
            ->where('pro_empresa',$idEmpresa)
            ->where('pro_tipo',1)
            ->get();
        return response()->json($allProductos);
    }

    public function getProductosIdPromocion($idPromocion){
 
        $allProductos = DB::table('promocion_producto')
            ->join('producto','promocion_producto.producto', '=', 'producto.pro_id')
            ->select('*')
            ->where('promocion',$idPromocion)
            ->where('prp_estado',1)
            ->get();
        return response()->json($allProductos);
    }

    public function getPromocionesEmpresa($idEmpresa){

        $allProductos = DB::table('producto')
            ->select('*')
            ->where('pro_empresa',$idEmpresa)
            ->where('pro_tipo',2)
            ->get();
        return response()->json($allProductos);
    }


    public function getKardexProducto($idProducto,$fechaInicio,$fechaFin){

        $allProductos = DB::table('kardex')
            ->select('*')
            ->where('ka_producto',$idProducto)
            ->whereBetween('ka_fecha_ingreso', [$fechaInicio, $fechaFin])
          
            ->get();
        return response()->json($allProductos);
    }

    public function updateProvedor(Request $request)
    {


        $response = DB::table('empresa')
            ->select('*')
            ->where('emp_id', $request->emp_id)
            ->update([
                        "emp_nombre" => $request->emp_nombre, 
                    ]);
   
    }


    public function updateCompra(Request $request)
    {

        $responseNew = DB::table('compra')
            ->select('*')
            ->where('com_id', $request->factura)
            ->update([
                        "com_tipo" => $request->accion, 
                    ]);

        if( $request->accion == 4){

             $allProductos = DB::table('detalle_compra')
                ->select('*')
                ->where('dcom_compra',$request->factura)
                ->get();

                foreach ($allProductos as $pro) {

                    $kardex = DB::table('kardex')->insert([
                        "ka_producto" => $pro->dcom_producto, 
                        "ka_tipo" => 1, 
                        "ka_cantidad" => $pro->dcom_cantidad, 
                        "ka_descripcion" => "Venta Cancelada",
                        "ka_fecha_ingreso" => now(), 
                        ]);
                    
                    $response = DB::table('producto')
                        ->select('*')
                        ->where('pro_id', $pro->dcom_producto)
                        ->first();
                                
                    $cantidad = $response->pro_stop;
                    
                    $cantidad = $cantidad + $request->pro_nuevo_stop;
                            
                    
                    $responseProducto = DB::table('producto')
                        ->select('*')
                        ->where('pro_id', $pro->dcom_producto)
                        ->update([
                                "pro_stop" => $cantidad, 
                                ]);
                    }


                        
        }
   
    
    }

    public function updateEmpleado(Request $request)
    {


        $response = DB::table('empleado')
            ->select('*')
            ->where('emp_id', $request->emp_id)
            ->update([
                        "emp_nombre" => $request->emp_nombre, 
                        "emp_apellido" => $request->emp_apellido, 
                        "emp_telefono" => $request->emp_telefono, 
                        "emp_usuario" => $request->emp_usuario, 
                        "emp_tipo" => $request->emp_tipo, 
                        "emp_contrasena" => $request->emp_contrasena, 
                    ]);
   
    }




    public function nuevoProducto(Request $request)
    {

        $producto = DB::table('producto')->insertGetId([
            "pro_nombre" => $request->pro_nombre, 
            "pro_empresa" => $request->pro_empresa, 
            "pro_precio_compra" => $request->pro_precio_compra, 
            "pro_precio_pvp" => $request->pro_precio_venta_publico, 
            "pro_precio_tienda" => $request->pro_precio_venta_tienda, 
            "pro_precio_distribuccion" => $request->pro_precio_venta_distribuidor, 
            "pro_empresa_distribuidora" => $request->pro_empresa_distribuidora, 
            "pro_url_imagen" =>"",
            "pro_stop" => 0,
            "pro_estado" => 1,
            "pro_tipo" =>1
        ]);


        return response()->json(["status" => true, "idProdcuto" => $producto]);
    }

    public function nuevoPromocion(Request $request)
    {

        $producto = DB::table('producto')->insertGetId([
            "pro_nombre" => $request->pro_nombre, 
            "pro_empresa" => $request->pro_empresa, 
            "pro_precio_compra" => $request->pro_precio_compra, 
            "pro_precio_pvp" => $request->pro_precio_venta_publico, 
            "pro_precio_tienda" => $request->pro_precio_venta_tienda, 
            "pro_precio_distribuccion" => $request->pro_precio_venta_distribuidor, 
            "pro_empresa_distribuidora" => $request->pro_empresa_distribuidora, 
            "pro_stop" => 0,
            "pro_estado" => 1,
            "pro_tipo" =>2
        ]);


        return response()->json(["status" => true, "idProdcuto" => $producto]);
    }

    public function itemProducto(Request $request)
    {

      
        $kardex = DB::table('kardex')->insert([
            "ka_producto" => $request->pro_id, 
            "ka_tipo" => $request->pro_tipo, 
            "ka_cantidad" => $request->pro_nuevo_stop, 
            "ka_fecha_ingreso" => $request->pro_fecha_ingreso, 
            "ka_descripcion" => "Carga mediante WEB",
        ]);

        $response = DB::table('producto')
            ->select('*')
            ->where('pro_id', $request->pro_id)
            ->first();
            
        $cantidad = $response->pro_stop;

        if($request->pro_tipo == 1){

            $cantidad = $cantidad + $request->pro_nuevo_stop;

        }else if($request->pro_tipo == 2){
            $cantidad = $cantidad - $request->pro_nuevo_stop;
        }

        $responseProducto = DB::table('producto')
        ->select('*')
        ->where('pro_id', $request->pro_id)
        ->update([
            "pro_stop" => $cantidad, 
            "pro_fecha_expiracion" => $request->pro_fecha_caducidad, 
                ]);


        $newresponse = DB::table('producto')
            ->select('*')
            ->where('pro_id', $request->pro_id)
            ->first();


        return response()->json(["status" => true, "producto" => $newresponse]);
    }

    public function updateproductos(Request $request)
    {

        $response = DB::table('producto')
        ->select('*')
        ->where('pro_id', $request->pro_id)
        ->update([
            "pro_nombre" => $request->pro_nombre, 
            "pro_empresa" => $request->pro_empresa, 
            "pro_precio_compra" => $request->pro_precio_compra, 
            "pro_precio_pvp" => $request->pro_precio_venta_publico, 
            "pro_precio_tienda" => $request->pro_precio_venta_tienda, 
            "pro_precio_distribuccion" => $request->pro_precio_venta_distribuidor, 
            "pro_url_imagen" => $request->pro_url_imagen
                ]);


        
    }



    public function updateProductoImagen(Request $request)
    {

        $response = DB::table('producto')
            ->select('*')
            ->where('pro_id', $request->pro_id)
            ->update([
                        "pro_url_imagen" => $request->pro_url_imagen, 
                    ]);
   
    }


    public function createOpciones(Request $request)
    {


        $id = DB::table('opciones')->insertGetId([
            
                "op_nombre" => $request->op_nombre,
                "op_url" => $request->op_url,
                "op_estado" => 1,
            
        ]);

        return response()->json(["status" => true, "success" => true, "message" => "Opcion Creada"]);

    }



    public function allOpciones()
    {
        $response = DB::table('opciones')
        ->select('*')
        ->get();
        
        return response()->json($response);
        
    }

    public function updateOpciones(Request $request)
    {
        $response = DB::table('opciones')
            ->select('*')
            ->where('op_codigo', $request->op_id)
            ->update([
                        'op_nombre' => $request->op_nombre,
                        'op_url' => $request->op_url,
                        'op_estado'=> $request->op_estado,
                      

                    ]);

        return response()->json(["status" => true, "success" => true, "message" => "Opcion actualizado"]);

        
    }


    public function createMenu(Request $request)
    {

        $id = DB::table('menu')->insertGetId([
                "MEN_NOMBRE" => $request->men_nombre,
                "MEN_DESCRIPCION" => $request->men_descripcion,
                "MEN_ICONO" => $request->men_icono,   
                "MEN_ESTADO" => 1,
                "MEN_MODULO" => $request->men_modulo,
               
        ]);

    
        return response()->json(["status" => true, "success" => true, "message" => "Menu exitoso"]);

    }

    public function getAllMenu()
    {
        $response = DB::table('menu')
            ->select('MEN_CODIGO','MEN_NOMBRE','MEN_DESCRIPCION','MEN_ICONO','MEN_ESTADO','MEN_MODULO')
            ->get();

        return response()->json($response);

    }


    public function createmodulos(Request $request)
    {

        $id = DB::table('modulos')->insertGetId([
            
                "MO_NOMBRE" => $request->mo_nombre,
                "MO_DESCRIPCION" => $request->mo_descripcion,
                "MO_ESTADO" => 1,
         

        ]);
 
        return response()->json(["status" => true, "success" => true, "message" => "Modulo creado con exitoso"]);
        

    }


    public function updateMenu(Request $request)
    {



        $response = DB::table('menu')
            ->select('*')
            ->where('MEN_CODIGO', $request->men_id)
            ->update([
                        'MEN_NOMBRE' => $request->men_nombre,
                        'MEN_DESCRIPCION' => $request->men_descripcion,
                        'MEN_ICONO' => $request->men_icono,
                        'MEN_ESTADO'=> $request->men_estado,
                        'MEN_MODULO'=> $request->men_modulo,
                   

                    ]);


        
        return response()->json(["status" => true, "success" => true, "message" => "Menu actualizado"]);

        
    }


    public function getListMenu($idMenu)
    {


        $listActivite = [];

        $responseOpciones = DB::table('opciones')
            ->select('*')
            ->where('op_estado',1)
            ->get();    

        $respondesAllActivatives  = DB::table('menu_opciones')
            ->select('*')
            ->where('MOP_ESTADO',1)
            ->get();   

    
        $listActiviteSelect = [];


        foreach ($responseOpciones as $opcion) {


            $flagAdd = true;

            foreach ($respondesAllActivatives as $menu) {

                if($opcion->op_codigo == $menu->MOP_OPCION){

                    $flagAdd = false;

                    if($menu->MOP_MENU == $idMenu){

                        array_unshift($listActivite, (object)['OP_CODIGO'=>$opcion->op_codigo,'OP_NOMBRE'=>$opcion->op_nombre,'OP_ESTADO'=> 1]);

                    }

                }

            

            }

            if($flagAdd){

                array_push($listActivite, (object)['OP_CODIGO'=>$opcion->op_codigo,'OP_NOMBRE'=>$opcion->op_nombre,'OP_ESTADO'=> 0]);
            
            }

           
        }

        
        return response()->json(["status" => true, "success" => true,"data" => $listActivite]);

    }

    public function createRol(Request $request)
    {

        $id = DB::table('roles')->insertGetId([
            
                "ROL_NOMBRE" => $request->rol_nombre,
                "ROL_DESCRIPCION" => $request->rol_descripcion,
                "ROL_ESTADO" => 1,

        ]);

       
        return response()->json(["status" => true, "success" => true, "message" => "Rol creado con exitoso"]);
        

    }


    public function getAllRol()
    {
        $response = DB::table('roles')
            ->select('ROL_CODIGO','ROL_NOMBRE','ROL_DESCRIPCION','ROL_ESTADO')
            ->get();
            

        return response()->json($response);
    }  


    public function activeOpcionesMenu(Request $request)
    {

        $response = DB::table('menu_opciones')
            ->select('*')
            ->where([
                        ['MOP_MENU', '=',  $request->mop_menu],
                        ['MOP_OPCION', '=', $request->mop_opcion],
                    ])
            -> first();

            
       if($response !== NULL){

            $responsed = DB::table('menu_opciones')
                ->select('*')
                ->where('MOP_CODIGO', $response->MOP_CODIGO)
                ->update([
                            'MOP_ESTADO'=> $request->mop_estado,
                        ]);

        
            return response()->json(["status" => true, "success" => true, "message" => "Opcion actualizado"]);

         

       }else{

            $id = DB::table('menu_opciones')->insertGetId([
                
                    "MOP_MENU" => $request->mop_menu,
                    "MOP_OPCION" => $request->mop_opcion,
                    "MOP_ESTADO" => $request->mop_estado,
       
            ]);

        
            return response()->json(["status" => true, "success" => true, "message" => "Opcion actualizado"]);

       }

        
       

    }


    public function updateRol(Request $request)
    {


        $response = DB::table('roles')
            ->select('*')
            ->where('ROL_CODIGO', $request->rol_id)
            ->update([
                        'ROL_NOMBRE' => $request->rol_nombre,
                        'ROL_DESCRIPCION' => $request->rol_descripcion,
                        'ROL_ESTADO' => $request->rol_estado,
                      

                    ]);

     
        return response()->json(["status" => true, "success" => true, "message" => "rol actualizado"]);

        
    }


    public function getRolesMenu($idRol){
        $listActivite = [];

        $response = DB::table('rol_menu')
            ->select('*')
            ->where('ROM_ROL',$idRol)
            ->get();


        $responseOpciones = DB::table('menu')
            ->select('*')
            ->where('MEN_ESTADO',1)
            ->get();    

        
            foreach ($responseOpciones as $menu) {

                $flgaActive = false;

                $responseOpcionesMenuOpcion = DB::table('menu_opciones')
                    ->join('opciones', 'opciones.op_codigo', '=', 'menu_opciones.MOP_OPCION')
                    ->select('*')
                    ->where('menu_opciones.MOP_ESTADO',1)
                    ->where('menu_opciones.MOP_MENU', $menu->MEN_CODIGO)
                    ->get();
                    
                $responseRolesMenuOpcions = DB::table('rol_menu_opcion')
                    ->select('*')
                    ->where('ROLMO_ROL',$idRol)
                    ->where('ROLMO_MENU',$menu->MEN_CODIGO)
                    ->get();



                $activeOpcionesRol = [];

                foreach ($responseOpcionesMenuOpcion as $opcionesMenu) {

                    $flagactiveOpcionesRol = false;


                    foreach ($responseRolesMenuOpcions as $rolemenu) {


                        if($opcionesMenu->op_codigo == $rolemenu->ROLMO_OPCIONES){

                            $flagactiveOpcionesRol = true;
                            array_push($activeOpcionesRol, (object)['OP_CODIGO'=>$opcionesMenu->op_codigo,'OP_NOMBRE'=>$opcionesMenu->op_nombre,'OP_ESTADO'=> $rolemenu->ROLMO_ESTADO]);

                        }

                    }

                    if(!$flagactiveOpcionesRol){
                        array_push($activeOpcionesRol, (object)['OP_CODIGO'=>$opcionesMenu->op_codigo,'OP_NOMBRE'=>$opcionesMenu->op_nombre,'OP_ESTADO'=> 0]);

                    }

                }

                foreach ($response as $rol) {

                    if($menu->MEN_CODIGO == $rol->ROM_MENU){

                        $flgaActive = true;

                        if($rol->ROM_ESTADO == 0){

                            array_push($listActivite, (object)['OP_CODIGO'=>$menu->MEN_CODIGO,'OP_NOMBRE'=>$menu->MEN_NOMBRE,'OP_ESTADO'=> 0,'LIST_OPCIONES'=> $activeOpcionesRol]);

                        } else if($rol->ROM_ESTADO == 1){

                            array_push($listActivite, (object)['OP_CODIGO'=>$menu->MEN_CODIGO,'OP_NOMBRE'=>$menu->MEN_NOMBRE,'OP_ESTADO'=> 1,'LIST_OPCIONES'=>$activeOpcionesRol]);
                        }
                        
                    }
                   

                }
                if(!$flgaActive){

                    array_push($listActivite, (object)['OP_CODIGO'=>$menu->MEN_CODIGO,'OP_NOMBRE'=>$menu->MEN_NOMBRE,'OP_ESTADO'=> 0,'LIST_OPCIONES'=>$activeOpcionesRol]);
                    $flgaActive = false;

                }

            }

            return response()->json(["status" => true, "success" => true,"data" => $listActivite]);



    }


    public function activeRolMenu(Request $request)
    {

        $response = DB::table('rol_menu')
            ->select('*')
            ->where([
                        ['ROM_ROL', '=',  $request->rom_rol],
                        ['ROM_MENU', '=', $request->rom_menu],
                    ])
            -> first();

            
       if($response !== NULL){

            $response = DB::table('rol_menu')
                ->select('*')
                ->where('ROM_CODIGO', $response->ROM_CODIGO)
                ->update([
                           

                            'ROM_ESTADO'=> $request->rom_estado,
                          

                        ]);


        
            return response()->json(["status" => true, "success" => true, "message" => "Rol actualizado"]);

         

       }else{

            $id = DB::table('rol_menu')->insert([
                
                    "ROM_ROL" => $request->rom_rol,
                    "ROM_MENU" => $request->rom_menu,
                    "ROM_ESTADO" => $request->rom_estado,
                 
                

            ]);

           
            return response()->json(["status" => true, "success" => true, "message" => "Rol actualizado"]);

       }

        

    }


    public function activeRolMenuOpcion(Request $request)
    {


        $response = DB::table('rol_menu')
            ->select('*')
            ->where([
                        ['ROM_ROL', '=',  $request->rolmo_rol],
                        ['ROM_MENU', '=', $request->rolmo_menu],
                    ])
            -> first();

            
       if($response !== NULL){

            $responsed = DB::table('rol_menu')
                ->select('*')
                ->where('ROM_CODIGO', $response->ROM_CODIGO)
                ->update([
                           

                            'ROM_ESTADO'=> 1,
                 

                        ]);
         

       }else{

            $id = DB::table('rol_menu')->insert([
                [
                    "ROM_ROL" => $request->rolmo_rol,
                    "ROM_MENU" => $request->rolmo_menu,
                    "ROM_ESTADO" => 1,
              
                ]

            ]);


       }

       $response = DB::table('rol_menu_opcion')
            ->select('*')
            ->where([
                        ['ROLMO_ROL', '=',  $request->rolmo_rol],
                        ['ROLMO_MENU', '=', $request->rolmo_menu],
                        ['ROLMO_OPCIONES', '=', $request->rolmo_opciones],
                       
                    ])
            -> first();

            
       if($response !== NULL){

            $responsed = DB::table('rol_menu_opcion')
                ->select('*')
                ->where('ROLMO_CODIGO', $response->ROLMO_CODIGO)
                ->update([
                           

                            'ROLMO_ESTADO'=> $request->rolmo_estado,
                    

                        ]);

            return response()->json(["status" => true, "success" => true, "message" => "Opcion actualizado en el rol"]);

       }else{

            $id = DB::table('rol_menu_opcion')->insertGetId([
                
                    "ROLMO_ROL" => $request->rolmo_rol,
                    "ROLMO_MENU" => $request->rolmo_menu,
                    "ROLMO_OPCIONES" => $request->rolmo_opciones,
                    "ROLMO_ESTADO" => $request->rolmo_estado,
                  
                

            ]);

           


            return response()->json(["status" => true, "success" => true, "message" => "Opcion actualizado en el rol"]);

       }
    }


    public function updateModulos(Request $request)
    {


        $response = DB::table('modulos')
            ->select('*')
            ->where('MO_CODIGO', $request->mo_id)
            ->update([
                        'MO_NOMBRE' => $request->mo_nombre,
                        'MO_DESCRIPCION' => $request->mo_descripcion,
                        'MO_ESTADO' => $request->mo_estado,
                   

                    ]);

        return response()->json(["status" => true, "success" => true, "message" => "Modulo actualizado"]);

        
    }


    public function createEmpresa(Request $request)
    {
        

        $creadaEmpresa = DB::table('empresa1')->insertGetId([
            
            "EMP_RAZON_SOCIAL" => $request->emp_razon_social,
            "EMP_NOMBRE_COMERCIAL" => $request->emp_nombre_comercial,
            "EMP_DIRECCION" => $request->emp_direccion,
            "EMP_TELEFONO" => $request->emp_telefono,
            "EMP_EMAIL_EMPRESA" => $request->emp_email,
            "EMP_REPRESENTANTE" => $request->emp_representante,
            "EMP_RUC" => $request->emp_ruc,
            "EMP_PAGINA_WEB" => $request->emp_pagina_web,
            "EMP_ESTADO" => 1,
      

        ]);

        return response()->json(["status" => true, "success" => true, "message" => "Empresa creada Correctamente"]);
    }

    public function getListEmpresas()
    {

        $responseEmpresas = DB::table('empresa1')
            ->select('*')
            ->get();


        return response()->json(["status" => true, "success" => true,"data" => $responseEmpresas]);

    }


    public function getallUser()
    {
        $response = DB::table('usuario')
            ->select('*')
            ->get();
            
        return response()->json($response);

    }

    public function updateUsuario(Request $request)
    {


        $response = DB::table('usuario')
            ->select('*')
            ->where('US_CODIGO', $request->us_id)
            ->update([

                    "US_CEDULA" => $request->us_cedula,
                    "US_NOMBRE" => $request->us_nombre,
                    "US_APELLIDO" => $request->us_apellido,
                    "US_DIRECCION" => $request->us_direccion,
                    "US_TELEFONO" => $request->us_telefono,
                    "US_CELULAR" => $request->us_celular,
                    "US_EMAIL" => $request->us_email,
                    "US_USUARIO" => $request->us_usuario,
                    "US_CONTRASENIA" => $request->us_contrasena,
                    "US_ESTADO" => $request->us_estado,

                    ]);


                
     
        return response()->json(["status" => true, "success" => true, "message" => "Usuario actualizado"]);

        
    }

    public function createUser(Request $request)
    {


        try {
            
            
            $id = DB::table('usuario')->insertGetId([
                
                    "US_CEDULA" => $request->us_cedula,
                    "US_NOMBRE" => $request->us_nombre,
                    "US_APELLIDO" => $request->us_apellido,
                    "US_DIRECCION" => $request->us_direccion,
                    "US_TELEFONO" => $request->us_telefono,
                    "US_CELULAR" => $request->us_celular,
                    "US_EMAIL" => $request->us_email,
                    "US_USUARIO" => $request->us_usuario,
                    "US_CONTRASENIA" => $request->us_contrasena,
                    "US_ESTADO" => $request->us_estado,
                    "US_EMAIL_TOKEN" => $request->us_email,
                    "US_TOKEN_PASS" => $request->us_cedula,
 
            ]);

            $user =new User();
            $user->name = $request->us_nombre." ".$request->us_apellido;
            $user->email = $request->us_usuario;
            $user->password = bcrypt($request->us_cedula);
            $user->idUsuario = $id;
            $user->save();
    
        
            return response()->json(["status" => true, "success" => true, "message" => "Usuario Creado"]);
        } catch (Throwable $e) {


            return response()->json(["status" => false, "success" => false, "message" => $e]);
        }
        

    }


    public function updateEmpresa(Request $request)
    {

        $creadaEmpresa = DB::table('empresa1')
            ->select('*')
            ->where('EMP_CODIGO', $request->emp_codigo)
            ->update([

                "EMP_RAZON_SOCIAL" => $request->emp_razon_social,
                "EMP_ESTADO" => $request->emp_estado,
                "EMP_NOMBRE_COMERCIAL" => $request->emp_nombre_comercial,
                "EMP_DIRECCION" => $request->emp_direccion,
                "EMP_TELEFONO" => $request->emp_telefono,
                "EMP_EMAIL_EMPRESA" => $request->emp_email,
                "EMP_REPRESENTANTE" => $request->emp_representante,
                "EMP_RUC" => $request->emp_ruc,
                "EMP_PAGINA_WEB" => $request->emp_pagina_web,
        ]);

    

        return response()->json(["status" => true, "success" => true, "message" => "Empresa actualizada Correctamente"]);
    }

      
    public function getAllModulos()
    {
        $response = DB::table('modulos')
            ->select('MO_CODIGO','MO_NOMBRE','MO_DESCRIPCION','MO_ESTADO')
            ->get();
            

        return response()->json($response);
    }


    
}
